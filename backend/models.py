from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os import environ
from typing import List, Dict

app = Flask(__name__)

PATH_VAR = 'DB_STRING'
USER = 'postgres'
PASSWORD = 'password'
DB_HOST = 'localhost:5432'
DB_NAME = 'classless_action'
DEFAULT_DB = f'postgresql://{USER}:{PASSWORD}@{DB_HOST}/{DB_NAME}'

app.config['SQLALCHEMY_DATABASE_URI'] = environ.get(PATH_VAR, DEFAULT_DB)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # suppress warning

db = SQLAlchemy(app)

# type alias
Column = db.Column

class BaseModel:
    def as_dict(self) -> Dict[str, Column]:
        pass

    @classmethod
    def cols_str(cls) -> List[str]:
        pass

    @classmethod
    def cols(cls) -> List[Column]:
        pass

class Company(db.Model, BaseModel):
    __tablename__ = 'companies'
    
    symbol = db.Column(db.String(5), nullable=False, primary_key=True)
    name = db.Column(db.String, nullable=False)    
    sector = db.Column(db.String)
    location = db.Column(db.String)
    founded = db.Column(db.String)
    description = db.Column(db.String)
    domain = db.Column(db.String)

    def as_dict(self) -> Dict[str, Column]:
        return {
            'symbol': self.symbol,
            'name': self.name,
            'sector': self.sector,
            'location': self.location,
            'founded': self.founded,
            'description': self.description,
            'domain': self.domain
        }

    @classmethod
    def cols_str(cls) -> List[str]:
        return [
            'symbol',
            'name',
            'sector',
            'location',
            'founded',
            'description',
            'domain'
        ]
    
    @classmethod
    def cols(cls) -> List[Column]:
        return [
            cls.symbol,
            cls.name,
            cls.sector,
            cls.location,
            cls.founded,
            cls.description,
            cls.domain
        ]
    
class Lawsuit(db.Model):
    __tablename__ = 'lawsuits'

    id = db.Column(db.Integer, nullable=False, primary_key=True)
    name = db.Column(db.String, nullable=False)
    company_symbol = db.Column(db.String(5), db.ForeignKey('companies.symbol'), nullable=False, primary_key=True)
    decision_date = db.Column(db.Date)
    court_name = db.Column(db.String)
    jurisdiction = db.Column(db.String)
    pdf = db.Column(db.String)

    def as_dict(self) -> Dict[str, List]:
        return {
            'id': self.id,
            'name': self.name,
            'company_symbol': self.company_symbol,
            'decision_date': self.decision_date,
            'court_name': self.court_name,
            'jurisdiction': self.jurisdiction,
            'pdf': self.pdf
        }
    
    @classmethod
    def cols_str(cls) -> List[str]:
        return [
            'id',
            'name',
            'company_symbol',
            'decision_date',
            'court_name',
            'jurisdiction',
            'pdf'
        ]
    
    @classmethod
    def cols(cls) -> List[Column]:
        return [
            cls.id,
            cls.name,
            cls.company_symbol,
            cls.decision_date,
            cls.court_name,
            cls.jurisdiction,
            cls.pdf
        ]
    
class Stock(db.Model):
    __tablename__ = 'stocks'

    symbol = db.Column(db.String(5), db.ForeignKey('companies.symbol'), primary_key=True, nullable=False)
    market_cap = db.Column(db.Float)
    shares = db.Column(db.BigInteger)
    epsForward = db.Column(db.Float)
    exchange = db.Column(db.String())
    averageDailyVolume10Day = db.Column(db.BigInteger)

    def as_dict(self) -> Dict[str, Column]:
        return {
            'symbol': self.symbol,
            'market_cap': self.market_cap,
            'shares': self.shares,
            'epsForward': self.epsForward,
            'exchange': self.exchange,
            'averageDailyVolume10Day': self.averageDailyVolume10Day
        }
    
    @classmethod
    def cols_str(cls) -> List[str]:
        return [
            'symbol',
            'market_cap',
            'shares',
            'epsForward',
            'exchange',
            'averageDailyVolume10Day'
        ]
    
    @classmethod
    def cols(cls) -> List[Column]:
        return [
            cls.symbol,
            cls.market_cap,
            cls.shares,
            cls.epsForward,
            cls.exchange,
            cls.averageDailyVolume10Day
        ]

class Stock_Date_Price(db.Model):
    __tablename__ = 'stock_date_prices'

    symbol = db.Column(db.String(5), db.ForeignKey('stocks.symbol'), primary_key=True, nullable=False)
    date = db.Column(db.Date, nullable=False, primary_key=True)
    closing_price = db.Column(db.Float, nullable=False)
    
    def as_dict(self) -> Dict[str, Column]:
        return {
            'symbol': self.symbol,
            'date': self.date,
            'closing_price': self.closing_price
        }
    
    @classmethod
    def cols_str(cls) -> List[str]:
        return [
            'symbol',
            'date',
            'closing_price'
        ]
    
    @classmethod
    def cols(cls) -> List[Column]:
        return [
            cls.symbol,
            cls.date,
            cls.closing_price
        ]

class Stock_Holders(db.Model):
    __tablename__ = 'stock_holders'
    
    symbol = db.Column(db.String(5), db.ForeignKey('stocks.symbol'), primary_key=True, nullable=False)
    holder_name = db.Column(db.String, primary_key=True, nullable=False)

    def as_dict(self) -> Dict[str, Column]:
        return {
            'symbol': self.symbol,
            'holder_name': self.holder_name
        }
    
    @classmethod
    def cols_str(cls) -> List[str]:
        return [
            'symbol',
            'holder_name'
        ]
    
    @classmethod
    def cols(cls) -> List[Column]:
        return [
            cls.symbol,
            cls.holder_name
        ]

db.create_all()