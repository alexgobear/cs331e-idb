import json
from models import app, db, Company, Stock, Stock_Date_Price, Lawsuit, Stock_Holders, BaseModel, Column
from sqlalchemy.sql import ClauseElement
from datetime import datetime, date
from data_scripts import getCompanyPillar
from threading import Thread

def populate():
    Thread(target=populate_companies).run()
    Thread(target=populate_lawsuits).start()
    Thread(target=populate_stocks).start()
    Thread(target=populate_stock_auxillaries).start()

def load_json(filename) -> dict:
    with open(filename) as file:
        jsn = json.load(file)

    return jsn

# https://stackoverflow.com/a/2587041/21345722
# will not create a model if it already exists
def get_or_create(session, model, defaults=None, **kwargs):
    # instance = session.query(model).filter_by(**kwargs).one_or_none()
    # if instance:
    #     return False

    params = {k: v for k, v in kwargs.items() if not isinstance(v, ClauseElement)}
    params.update(defaults or {})
    instance = model(**params)
    try:
        session.add(instance)
        session.commit()
    except Exception:  # The actual exception depends on the specific database so we catch all exceptions. This is similar to the official documentation: https://docs.sqlalchemy.org/en/latest/orm/session_transaction.html
        session.rollback()
        return False
    else:
        return True

# populate companies table
def populate_companies():
    companies_json = load_json('api_data/company.json')
    domain_json = load_json('api_data/domain.json')

    # list of tuples, each tuple of length 1
    existingCompanies = db.session.query(Company.symbol).all()

    print("Populating companies table")

    # companies = []
    # for companySymbol, company in companies_json.items():
    #     if (companySymbol,) in existingCompanies: # duplicate
    #         continue
        
    #     # create company object
    #     newCompany = Company(
    #         symbol = companySymbol,
    #         name = company["Name"],
    #         sector = company["Sector"],
    #         location = company["Location"],
    #         founded = company["Founded"],
    #         description = company["Company_Description"],
    #         domain = domain_json[companySymbol]
    #     )

    #     # get_or_create(db.session, Company, **newCompany.as_dict())

    #     # commit the session to database
    #     db.session.add(newCompany)
    #     db.session.commit()

    #     # keep track of progress
    #     # if idx % 10 == 0:
    #     #     print(f"{round((idx/len(companies_json))*100, 2)}% Complete")

    companies = [
        Company(
            symbol = companySymbol,
            name = company["Name"],
            sector = company["Sector"],
            location = company["Location"],
            founded = company["Founded"],
            description = company["Company_Description"],
            domain = domain_json[companySymbol]
        ).as_dict() 
        for companySymbol, company in companies_json.items() 
        if (companySymbol,) not in existingCompanies
    ]

    db.session.bulk_insert_mappings(Company, companies)
    db.session.commit()

    print('Populating companies complete')


# populate stocks table
def populate_stocks():
    path_to_json = 'api_data/additionalStockInfo.json'
    stocks_json = load_json(path_to_json)

    existingStocks = db.session.query(Stock.symbol).all()

    print("Populating Stocks table")
    # for companySymbol in stocks_json:
    #     if (companySymbol,) in existingStocks: # duplicate
    #         continue

    #     newStock = Stock(
    #         symbol = companySymbol,
    #         market_cap = stocks_json[companySymbol]["marketCap"], 
    #         shares =  stocks_json[companySymbol]["sharesOutstanding"]
    #     )

    #     # add to session
    #     get_or_create(db.session, Stock, **newStock.as_dict())

    #     # commit the session to database
    #     db.session.commit()

    #     # keep track of progress
    #     # if idx1 % 10 == 0:
    #     #     print(f"{round((idx1/len(stocks_json))*100, 2)}% Complete")

    stocks = (
        Stock(
            symbol = companySymbol,
            market_cap = stock["marketCap"], 
            shares =  stock["sharesOutstanding"],
            epsForward = stock["epsForward"] if "epsForward" in stock else 0,
            exchange = stock["exchange"],
            averageDailyVolume10Day = stock["averageDailyVolume10Day"]
        ).as_dict()
        for companySymbol, stock in stocks_json.items()
        if (companySymbol,) not in existingStocks
    )
    db.session.bulk_insert_mappings(Stock, stocks)
    db.session.commit()
    
    print('Populating Stocks complete')

# populate Stock_Date_Price and Stock_Holders tables using batch processing
def populate_stock_auxillaries():
    path_to_json = 'api_data/stock.json'
    stocks_json = load_json(path_to_json)

    existingPoints = db.session.query(Stock_Date_Price.symbol, Stock_Date_Price.date).all()
    existingHolders = db.session.query(Stock_Holders.symbol, Stock_Holders.holder_name)

    print("Populating Stock holders and date/prices")

    stock_points = []
    stock_holders = []
    for companySymbol, stock in stocks_json.items():
        newStockHolder = {
            'symbol': companySymbol,
            'holder_name': stock["First_Institutional_Holder"]
        }

        if (companySymbol, newStockHolder['holder_name']) in existingHolders:
            print(f'duplicate found: {companySymbol}, {newStockHolder["holder_name"]}')
            continue

        stock_holders.append(newStockHolder)

        for i in range(len(stock["date"])):
            # create object
            newStockPoint = {
                'symbol': companySymbol,
                'date': datetime.strptime(stock["date"][i][:10], "%Y-%m-%d").date(),
                'closing_price': stock["closingPrice"][i]
            }

            if (companySymbol, newStockPoint['date']) in existingPoints:
                continue

            stock_points.append(newStockPoint)
        
        # keep track of progress
        # if idx % 10 == 0:
        #     print(f"{round((idx/len(stocks_json))*100, 2)}% Complete")

    

    # insert all stock points in one bulk operation
    db.session.bulk_insert_mappings(Stock_Holders, stock_holders)
    db.session.commit()
    print("Populating Stock holders complete.")

    db.session.bulk_insert_mappings(Stock_Date_Price, stock_points)
    db.session.commit()
    print('populating stock date/prices complete')
    

# populate lawsuits table
def populate_lawsuits():
    # create cipher
    companyCipher = getCompanyPillar.createCompanySymbolCipher()
    getSymbol = lambda x: list(companyCipher.keys())[list(companyCipher.values()).index(x)]

    path_to_json = 'api_data/lawsuitsV2.json'
    allSpxLawsuits = load_json(path_to_json)

    existingLawsuits = db.session.query(Lawsuit.id, Lawsuit.company_symbol).all()

    print("Populating Lawsuits table")
    # lawsuits = []
    # for companyName in allSpxLawsuits:
    #     lawsuitsOneCompany = allSpxLawsuits[companyName]
    #     for lawsuit in lawsuitsOneCompany:
            
    #         newLawsuit = {
    #             'id': lawsuit["caseLawId"],
    #             'name': lawsuit["caseName"],
    #             'company_symbol': list(companyCipher.keys())[list(companyCipher.values()).index(companyName)],
    #             'decision_date': formatDate(lawsuit["decisionDate"]),
    #             'court_name': lawsuit["courtName"],
    #             'jurisdiction': lawsuit["jurisdictionName"],
    #             'pdf': lawsuit["pdf"]
    #         }
            
    #         if (newLawsuit['id'], newLawsuit['company_symbol']) in existingLawsuits:
    #             continue

    #         lawsuits.append(newLawsuit)
        
        # keep track of progress
        # if idx % 10 == 0:
        #     print(f"{round((idx/len(allSpxLawsuits))*100, 2)}% Complete")

    lawsuits = (
        Lawsuit(
            id = lawsuit["caseLawId"],
            name = lawsuit["caseName"],
            company_symbol = getSymbol(companyName),
            decision_date = formatDate(lawsuit["decisionDate"]),
            court_name = lawsuit["courtName"],
            jurisdiction = lawsuit["jurisdictionName"],
            pdf = lawsuit["pdf"]
        ).as_dict()
        for companyName, company in allSpxLawsuits.items()
        for lawsuit in company
        if (lawsuit['caseLawId'], getSymbol(companyName)) not in existingLawsuits
    )

    db.session.bulk_insert_mappings(Lawsuit, lawsuits)
    db.session.commit()
    print("Populating lawsuits complete.")


def formatDate(date_str: str) -> date:
    if len(date_str) == 7: # ex: 1894-01
        date_str = date_str + "-01"
    elif len(date_str) == 4:
        date_str = date_str + "-01-01"

    return datetime.strptime(date_str, "%Y-%m-%d").date()


if __name__ == '__main__':
    populate()
