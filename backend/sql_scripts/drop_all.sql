-- Drops all tables from current database
DROP TABLE companies CASCADE;
DROP TABLE lawsuits CASCADE;
DROP TABLE stocks CASCADE;
DROP TABLE stock_date_prices CASCADE;
DROP TABLE stock_holders CASCADE;