from unittest import main, TestCase
from models import db, Company, Lawsuit, Stock, Stock_Date_Price, Stock_Holders
from datetime import datetime

session = db.session

class TestModels(TestCase):
    def test_company_1(self):
        c = Company(
            symbol='TEST',
            name='Test'
        )

        session.add(c)
        session.commit()

        res = session.query(Company).filter_by(symbol = 'TEST')
        self.assertEqual(res.one().symbol, 'TEST')

        res.delete()
        session.commit()

    def test_lawsuit_1(self):
        case = Lawsuit(
            id=123123,
            name='Test',
            company_symbol='TEST'
        )
        c = Company(
            symbol='TEST',
            name='Test'
        )
        
        session.add(c)
        session.add(case)
        
        session.commit()

        res = session.query(Lawsuit).filter_by(id=123123)
        self.assertEqual(res.one().company_symbol, 'TEST')

        res.delete()
        session.query(Company).filter_by(symbol='TEST').delete()
        session.commit()

    def test_stock_1(self):
        c = Company(
            symbol='TEST',
            name='Test'
        )
        s = Stock(
            symbol='TEST',
            market_cap=1006694739540.4453,
            shares=10247300096
        )

        session.add_all((c, s))
        session.commit()

        res = session.query(Stock).filter_by(symbol='TEST')
        self.assertEqual(res.one().market_cap, 1006694739540.4453)

        res.delete()
        session.query(Company).filter_by(symbol='TEST').delete()
        session.commit()

    def test_stock_dp_1(self):
        c = Company(
            symbol='TEST',
            name='Test'
        )
        s = Stock(
            symbol='TEST',
            market_cap=1006694739540.4453,
            shares=10247300096
        )
        sdp = Stock_Date_Price(
            symbol='TEST',
            date=datetime.strptime("2010-06-29 00:00:00-0400", "%Y-%m-%d %H:%M:%S%z"),
            closing_price=1.5926669836044312
        )

        session.add_all((c, s, sdp))
        session.commit()

        res = session.query(Stock_Date_Price).filter_by(symbol='TEST')
        self.assertEqual(res.one().closing_price, 1.5926669836044312)

        res.delete()
        session.query(Stock).filter_by(symbol='TEST').delete()
        session.query(Company).filter_by(symbol='TEST').delete()
        
        session.commit()

    def test_stock_holder_1(self):
        c = Company(
            symbol='TEST',
            name='Test'
        )
        s = Stock(
            symbol='TEST',
            market_cap=1006694739540.4453,
            shares=10247300096
        )
        sh = Stock_Holders(
            symbol='TEST',
            holder_name='TEST HOLDER'
        )

        session.add_all((c, s, sh))
        session.commit()

        res = session.query(Stock_Holders).filter_by(symbol='TEST')
        self.assertEqual(res.one().holder_name, 'TEST HOLDER')

        res.delete()
        session.query(Stock).filter_by(symbol='TEST').delete()
        session.query(Company).filter_by(symbol='TEST').delete()

        session.commit()

    # updating
    def test_company_2(self):
        c = Company(
            symbol = "BRK.B",
            name = "Berkshire Hathaway Inc. Class B",
            sector = "Financials",
            location = "Omaha, Nebraska",
            founded = "1839",
            description = "Berkshire Hathaway Inc. (/\u02c8b\u025c\u02d0rk\u0283\u0259r/) is an American multinational conglomerate holding company headquartered in Omaha, Nebraska, United States. Its main business and source of capital is insurance, from which it invests the float (the retained premiums) in a broad portfolio of subsidiaries, equity positions and other securities. The company has been overseen since 1965 by its chairman and CEO Warren Buffett and (since 1978) vice chairman Charlie Munger, who are known for their advocacy of value investing principles. Under their direction, the company's book value has grown at an average rate of 20%, compared to about 10% from the S&P 500 index with dividends included over the same period, while employing large amounts of capital and minimal debt.[4]"
        )

        session.add(c)
        session.commit()

        res = session.query(Company).filter_by(symbol = 'BRK.B')
        self.assertEqual(res.one().founded, '1839')
        
        # update value from 1839 to 2023
        session.query(Company).update({Company.founded: "2023"})
        session.commit()

        # check whether value was updated in the table
        self.assertEqual(res.one().founded, "2023")

        res.delete()
        session.commit()

    def test_lawsuit_2(self):
        case = Lawsuit(
            id=123123,
            name='Test',
            company_symbol='TEST',
            court_name = "Travis County Court"
        )
        c = Company(
            symbol='TEST',
            name='Test'
        )
        
        session.add(c)
        session.add(case)
        
        session.commit()

        res = session.query(Lawsuit).filter_by(id=123123)
        self.assertEqual(res.one().company_symbol, 'TEST')
        self.assertEqual(res.one().court_name, "Travis County Court")

        session.query(Lawsuit).update({Lawsuit.court_name: "Bevo's Court House"})
        self.assertEqual(res.one().court_name, "Bevo's Court House")

        res.delete()
        session.query(Company).filter_by(symbol='TEST').delete()
        session.commit()

    def test_stock_2(self):
        c = Company(
            symbol='TEST',
            name='Test'
        )
        s = Stock(
            symbol='TEST',
            market_cap=1006694739540.4453,
            shares=10247300096
        )

        session.add_all((c, s))
        session.commit()

        res = session.query(Stock).filter_by(symbol='TEST')
        self.assertEqual(res.one().market_cap, 1006694739540.4453)
        self.assertEqual(res.one().shares, 10247300096)

        res.update({Stock.market_cap: 200, Stock.shares:1000})
        self.assertEqual(res.one().market_cap, 200)
        self.assertEqual(res.one().shares, 1000)

        res.delete()
        session.query(Company).filter_by(symbol='TEST').delete()
        session.commit()
    
    def test_stock_dp_2(self):
        c = Company(
            symbol='APPL',
            name='Apple Inc.'
        )

        s = Stock(
            symbol='APPL',
            market_cap=1006694739540.4453,
            shares=10247300096
        )

        sdp = Stock_Date_Price(
            symbol = "APPL",
            date = datetime.date(datetime.strptime("1980-12-12", "%Y-%m-%d")),
            closing_price = 0.09972158819437027
        )
        
        session.add_all((c, s, sdp))
        session.commit()
        res = session.query(Stock_Date_Price).filter_by(symbol = 'APPL')
        self.assertEqual(res.one().symbol, 'APPL')
        dt = datetime(1980, 12, 12)
        self.assertEqual(res.one().date, dt.date())
        self.assertEqual(res.one().closing_price, 0.09972158819437027)

        # update date and closing pricee
        new_dt = datetime(2008, 9, 29)
        session.query(Stock_Date_Price).update({Stock_Date_Price.date: new_dt.date(), Stock_Date_Price.closing_price: 50.5})
        session.commit()

        # check whether value was updated in the table
        self.assertEqual(res.one().date, new_dt.date())
        self.assertEqual(res.one().closing_price, 50.5)

        res.delete()
        session.query(Stock).filter_by(symbol='APPL').delete()
        session.query(Company).filter_by(symbol='APPL').delete()

        session.commit()

    def test_stock_holder_2(self):
        c = Company(
            symbol='TEST',
            name='Test'
        )
        s = Stock(
            symbol='TEST',
            market_cap=1006694739540.4453,
            shares=10247300096
        )
        sh = Stock_Holders(
            symbol='TEST',
            holder_name='TEST HOLDER'
        )

        session.add_all((c, s, sh))
        session.commit()

        res = session.query(Stock_Holders).filter_by(symbol='TEST')
        self.assertEqual(res.one().holder_name, 'TEST HOLDER')

        res.update({Stock_Holders.holder_name: 'Different Holder'})
        session.commit()
        self.assertEqual(res.one().holder_name, 'Different Holder')

        res.delete()
        session.query(Stock).filter_by(symbol='TEST').delete()
        session.query(Company).filter_by(symbol='TEST').delete()

        session.commit()

    # deleting
    def test_company_3(self):
        c = Company(
            symbol = "BRK.B",
            name = "Berkshire Hathaway Inc. Class B",
            sector = "Financials",
            location = "Omaha, Nebraska",
            founded = "1839",
            description = "Berkshire Hathaway Inc. (/\u02c8b\u025c\u02d0rk\u0283\u0259r/) is an American multinational conglomerate holding company headquartered in Omaha, Nebraska, United States. Its main business and source of capital is insurance, from which it invests the float (the retained premiums) in a broad portfolio of subsidiaries, equity positions and other securities. The company has been overseen since 1965 by its chairman and CEO Warren Buffett and (since 1978) vice chairman Charlie Munger, who are known for their advocacy of value investing principles. Under their direction, the company's book value has grown at an average rate of 20%, compared to about 10% from the S&P 500 index with dividends included over the same period, while employing large amounts of capital and minimal debt.[4]"
        )

        session.add(c)
        session.commit()

        res = session.query(Company)
        self.assertEqual(res.count(), 1)
        
        # delete record
        res.delete()
        session.commit()

        self.assertEqual(res.count(), 0)

    def test_lawsuit_3(self):
        case = Lawsuit(
            id=123123,
            name='Test',
            company_symbol='TEST',
            court_name = "Travis County Court"
        )
        c = Company(
            symbol='TEST',
            name='Test'
        )
        
        session.add(c)
        session.add(case)
        
        session.commit()

        res = session.query(Lawsuit)
        res2 = session.query(Company)
        self.assertEqual(res.count(), 1)

        res.delete()
        res2.delete()
        session.commit()

        self.assertEqual(res.count(), 0)
        self.assertEqual(res2.count(), 0)

    def test_stock_3(self):
        c = Company(
            symbol='TEST',
            name='Test'
        )
        s = Stock(
            symbol='TEST',
            market_cap=1006694739540.4453,
            shares=10247300096
        )

        session.add_all((c, s))
        session.commit()

        res = session.query(Stock)
        res2 = session.query(Company)
        self.assertEqual(res.count(), 1)
        self.assertEqual(res.count(), 1)

        res.delete()
        res2.delete()
        session.commit()

        self.assertEqual(res.count(), 0)
        self.assertEqual(res2.count(), 0)

    def test_stock_dp_3(self):
        c = Company(
            symbol='APPL',
            name='Apple Inc.'
        )

        s = Stock(
            symbol='APPL',
            market_cap=1006694739540.4453,
            shares=10247300096
        )

        sdp = Stock_Date_Price(
            symbol = "APPL",
            date = datetime.date(datetime.strptime("1980-12-12", "%Y-%m-%d")),
            closing_price = 0.09972158819437027
        )
        
        session.add_all((c, s, sdp))
        session.commit()

        res = session.query(Stock_Date_Price)
        res2 = session.query(Stock)
        res3 = session.query(Company)


        self.assertEqual(res.count(), 1)
        self.assertEqual(res2.count(), 1)
        self.assertEqual(res3.count(), 1)

        res.delete()
        res2.delete()
        res3.delete()
        session.commit()

        self.assertEqual(res.count(), 0)
        self.assertEqual(res2.count(), 0)
        self.assertEqual(res3.count(), 0)

    def test_stock_holder_3(self):
        c = Company(
            symbol='TEST',
            name='Test'
        )
        s = Stock(
            symbol='TEST',
            market_cap=1006694739540.4453,
            shares=10247300096
        )
        sh = Stock_Holders(
            symbol='TEST',
            holder_name='TEST HOLDER'
        )

        session.add_all((c, s, sh))
        session.commit()

        res = session.query(Stock_Holders)
        res2 = session.query(Stock)
        res3 = session.query(Company)

        self.assertEqual(res.count(), 1)
        self.assertEqual(res2.count(), 1)
        self.assertEqual(res3.count(), 1)

        res.delete()
        res2.delete()
        res3.delete()
        session.commit()

        self.assertEqual(res.count(), 0)
        self.assertEqual(res2.count(), 0)
        self.assertEqual(res3.count(), 0)

if __name__ == '__main__':
    main()