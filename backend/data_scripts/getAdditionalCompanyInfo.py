import requests
from getCompanyPillar import createCompanySymbolCipher
import json

def convert_to_2d_list(lst):
    two_d_list = []
    sublist = []
    for i in range(len(lst)):
        sublist.append(lst[i])
        if len(sublist) == 10:
            two_d_list.append(sublist)
            sublist = []
    if sublist:
        two_d_list.append(sublist)
    print("completed conversion of 2D list")
    return two_d_list

def makeApiCall(two_d_list):
    print("beginning API calls")
    temp_dictionary = {}
    for idx, list_ in enumerate(two_d_list):

        url = "https://yfapi.net/v6/finance/quote"

        querystring = {"symbols":",".join(list_)}

        headers = {
            'x-api-key': "bDOFRe9wm86imiTLX3yEi4OhG5Eu3mBt7cyHYMvb"
            }

        response = requests.request("GET", url, headers=headers, params=querystring)
        data = response.json()
        list_of_dictionaries = data["quoteResponse"]["result"]

        # make api call
        # save company stock info into a dictionary
        for dictionary in list_of_dictionaries:
            temp_dictionary[dictionary["symbol"]] = dictionary

    # keep track of results
        if idx % 10 == 0:
            print(f"{round((idx/len(two_d_list))*100,2)}% Complete")

    return temp_dictionary


if __name__ != "main":
    tickers = list(createCompanySymbolCipher().keys())
    tickers_2d = convert_to_2d_list(tickers)

    complete_dictionary = makeApiCall(tickers_2d)

    # convert dictionary to json 
    with open('api_data/additionalStockInfo.json', 'w') as fp:
        json.dump(complete_dictionary, fp, indent=4)
