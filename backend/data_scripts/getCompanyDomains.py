from googlesearch import search
from getCompanyPillar import createCompanySymbolCipher
import json
import requests
from io import BytesIO
from PIL import Image

# constants
USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3"

# List of company names
companies = list(createCompanySymbolCipher().values())
tickers = list(createCompanySymbolCipher().keys())
companyCipher = createCompanySymbolCipher()
newCompanyNames = []
domainNames = {}

# remove "Class" from string
for index in range(len(companies)):
    company = companies[index]
    if "Class" in company:
        endIndex = company.index("Class") - 1
        newCompanyNames.append(company[:endIndex])
    else:
        newCompanyNames.append(company)

# newCompanyNames = ["Mastercard Incorporated"]

for i, company in enumerate(newCompanyNames):
    query = f"{company} main site"
    for address in search(query, num_results=10):
        url = address.strip()
        if '://' in url:
            startIdx = url.index('://') + 3
            url = url[startIdx:]
        if '.com' in url:
            url = url[:url.index('.com')+4]
        # print(address)
        if "wikipedia" not in url and "bloomberg" not in url and "finance" not in url:
            domainNames[tickers[i]] = url 

            # define the URL to capture
            # send a request to the URL and get the response
            try:
                response = requests.get(f"https://logo.clearbit.com/{url}", headers={"User-Agent": USER_AGENT}, timeout=120)
                
                # open the response content as an image using Pillow
                image = Image.open(BytesIO(response.content))
                
                # edit name so that it has underscores

                # save the image as a PNG file
                image.save(f"api_data/companyLogos/{tickers[i]}.png", "PNG")
            except Exception as e:
                response = requests.get(f"https://logo.clearbit.com/finance.yahoo.com/", headers={"User-Agent": USER_AGENT}, timeout=120)
                # open the response content as an image using Pillow
                image = Image.open(BytesIO(response.content))
                
                # edit name so that it has underscores

                # save the image as a PNG file
                image.save(f"api_data/companyLogos/{tickers[i]}.png", "PNG")

                # open the response content as an image using Pillow
                image = Image.open(BytesIO(response.content))
            
            # print(company + ' domain name: ' + url)
            break
    # keep track of results
    if i % 10 == 0:
        print(f"{round((i/len(newCompanyNames))*100,2)}% Complete")

# turn dictionary into json
with open('api_data/domain.json', 'w') as fp:
    json.dump(domainNames, fp, indent=4)