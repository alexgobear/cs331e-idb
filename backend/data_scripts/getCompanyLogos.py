import requests
import re
from getCompanyPillar import createCompanySymbolCipher
import traceback
from io import BytesIO
from PIL import Image

# constants
USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3"

def getCompanyLogo(name):
    # processing technique 
    # remove elements in list that are of length 1
    arr = name.split()
    i = 0
    while i < len(arr):
        if len(arr[i]) == 1 or len(arr[i]) == 2:
            arr.pop(i)
        elif arr[i] == "Class":
            arr.pop(i)
        else:
            i += 1

    # join string elements using an underscore
    newName = "_".join(arr)
    # print(newName) #debugging
    url = f"https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&srsearch={newName}"
    response = requests.get(url, timeout=120)
    # print(response.json()) # debugging
    data = response.json()

    search_results = data["query"]["search"]
    if search_results:
        page_title = search_results[0]["title"]  # Use the first search result as the page title
        page_id = search_results[0]["pageid"]  # Use the page ID to retrieve the images

        url = f"https://en.wikipedia.org/w/api.php?action=query&format=json&prop=images&pageids={page_id}&imlimit=max"
        response = requests.get(url, timeout=180)
        data = response.json()
        print(data)
        try:
            images = data["query"]["pages"][str(page_id)].get("images", [])
            # if the "images" key is not found, return an empty list instead
        except Exception as e:
            traceback.print_exc()
            print(len(imageUrls))

        logo_title = None
        print(images)
        for image in images:
            if re.search(r"\blogo\b", image["title"], re.IGNORECASE) and image["title"].endswith(".svg") and not re.search(r"Commons", image["title"]):
                logo_title = image["title"]
                break
        if not logo_title:
            nameArr = re.split(r"\s|-|_", newName)
            firstPortionOfName, *secondPortionOfName = nameArr # tuple unpacking
            if len(secondPortionOfName) > 0:
                secondPortionOfName = secondPortionOfName[0]
            else:
                secondPortionOfName = ""
            for image in images:
                    if re.search(rf"{firstPortionOfName}", image["title"], re.IGNORECASE) and re.search(rf"{secondPortionOfName}", image["title"], re.IGNORECASE) and image["title"].endswith((".svg", ".png", ".jpg")):
                        logo_title = image["title"]
                        break
            if not logo_title:
                for image in images:
                    if re.search(rf"{firstPortionOfName}", image["title"], re.IGNORECASE) and image["title"].endswith((".svg", ".png", ".jpg")):
                        logo_title = image["title"]
                        break
            if not logo_title:
                for image in images:
                    if re.search(rf"{secondPortionOfName}", image["title"], re.IGNORECASE) and image["title"].endswith((".svg", ".png", ".jpg")):
                        logo_title = image["title"]
                        break
            if not logo_title:
                for image in images:
                    if image["title"].endswith((".jpg", ".png", ".PNG", ".gif")):
                        logo_title = image["title"]
                        break
    
    return logo_title

# create a list of stock symbols
spx500Names = createCompanySymbolCipher().values()
spx500Names = list(spx500Names)
spx500Names[spx500Names.index("Cooper Companies Inc.")] = "The_Cooper_Companies"
imageUrls = []
spx500Names = ["Tesla Inc."]

# using logo_title get an api request that get url to logo
for idx, name in enumerate(spx500Names):
    print(name)
    logo_title = getCompanyLogo(name)

    if logo_title:
        # check if url encoding is required EX: & => %26
        if '&' in logo_title:
            logo_title = logo_title.replace("&", "%26")
        try:
            results = requests.get(f"https://en.wikipedia.org/w/api.php?action=query&titles={logo_title}&prop=pageimages&format=json&pithumbsize=500&piprop=thumbnail")
            wikipediaImageSource = list(results.json()["query"]["pages"].values())[0]["thumbnail"]["source"]
            print(wikipediaImageSource)
            imageUrls.append(wikipediaImageSource) # to double check output
            # define the URL to capture
            # send a request to the URL and get the response
            response = requests.get(wikipediaImageSource, headers={"User-Agent": USER_AGENT})

            # open the response content as an image using Pillow
            image = Image.open(BytesIO(response.content))
            
            # edit name so that it has underscores
            my_list = name.split()
            separator = "_"
            filename = separator.join(my_list)

            # save the image as a PNG file
            image.save(f"../api_data/companyLogos/{filename}.png", "PNG")
        except Exception as e:
            imageUrls.append("No logo")
            print(f"{name} prop request did not work (image source not found)")
    elif logo_title == "Bad search Result":
        # imageUrls.append("No logo")
        print(f"{name} No search results found")
    else:
        # imageUrls.append("No Logo")
        print(f"{name} Logo not found")

    # keep track of results
    if idx % 10 == 0:
        print(f"{round((idx/len(spx500Names))*100,2)}% Complete")

print(imageUrls)
# print(len(imageUrls))
# print(len(spx500Names))