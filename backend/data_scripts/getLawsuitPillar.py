import requests
import json
from getCompanyPillar import createCompanySymbolCipher
from tqdm import tqdm

def getSampleData():
    # data dictionary
    companies = {}
    spx500 = list(createCompanySymbolCipher().values())
    alternateCompanyName = {
        "Alphabet Inc. Class A": "Google Inc.",
        "Alphabet Inc. Class C": "Google Inc.",
        "Johnson & Johnson": "Johnson & Johnson pharmaceuticals",
        "Meta Platforms Inc. Class A": "Facebook",
        "Procter & Gamble Company": "Procter & Gamble Co.",
    }
    alternateCompanyKeys = alternateCompanyName.keys()
    print(spx500) # print list to get an idea of endpoint structure

    for idx, company_name in enumerate(spx500):
        company = []
        apiCompanyName = company_name

        # conditions to improve search endpoint
        if company_name in alternateCompanyKeys:
            apiCompanyName = alternateCompanyName[company_name]

        # send api request
        response = requests.get(
            f'https://api.case.law/v1/cases/?search={apiCompanyName}',
            headers={'Authorization': 'b146d2a7f25f48d5a6693630d3b8dc995421f30c'}
        )

        classActionDict= response.json()

        # get information from caseLaw API
        lawsuits = classActionDict["results"]

        lawsuitCount = 0
        amountOfLawsuits = min(classActionDict['count'], 10)
        for i in range(amountOfLawsuits):
            if lawsuitCount == amountOfLawsuits:
                break
            temp_lawsuit_dict = {}

            lawsuit = lawsuits[i]

            # temp_lawsuit_dict["caseNumber"] = i + 1
            temp_lawsuit_dict['caseLawId'] = lawsuit['id']
            temp_lawsuit_dict["numberOfClassActionLawsuits"] = classActionDict['count']
            temp_lawsuit_dict['caseName'] = lawsuit['name_abbreviation']
            temp_lawsuit_dict['decisionDate'] = lawsuit['decision_date']
            temp_lawsuit_dict['courtName'] = lawsuit["court"]["name"]
            temp_lawsuit_dict['jurisdictionName'] = lawsuit["jurisdiction"]["name"]
            temp_lawsuit_dict['pdf'] = lawsuit["frontend_pdf_url"]
            company.append(temp_lawsuit_dict)
            lawsuitCount += 1
        
        # Progress tracker
        if idx % 10 == 0:
            print(f"{round((idx/len(spx500))*100, 2)}% Complete")
        companies[company_name] = company

    return companies

if __name__ == '__main__':
    class_action_dictionary = getSampleData()
    # print(json.dumps(company_dictionary, indent=4))
    with open('api_data/lawsuitsV2.json', 'w') as fp:
        json.dump(class_action_dictionary, fp, indent=4)