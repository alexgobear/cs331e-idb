import pandas as pd
import requests
from bs4 import BeautifulSoup
import json
import os

def createCompanySymbolCipher(): 
    # convert html table to a dataframe
    # print(os.getcwd())
    listOfDataFrames = pd.read_html('api_data/stockTable.html') # list of dataframes
    df = listOfDataFrames[0]
    df = df[["Security", "Symbol"]]

    # create two lists where keys represent the stock symbol and values represent the company name
    symbolList = df["Symbol"].tolist()
    companyList = df["Security"].tolist()
    symbolCompanyCipher = {}

    # reformat symbol so it adheres to Ticker object expected input ex: BF.B => BF-B
    for i in range(len(symbolList)):
        if symbolList[i] == "BRK.B":
            symbolList[i] = "BRK-B"
        else:
            if symbolList[i] == "BF.B":
                symbolList[i] = "BF-B"
            else:
                continue

    # create dictionary 
    for index in range(len(symbolList)):
        symbolCompanyCipher[symbolList[index]] = companyList[index]
    return symbolCompanyCipher

def getCompanyUrlFromWikipedia():
    print("Scraping company urls from Wikipedia...")
    tableUrl = "https://en.wikipedia.org/wiki/List_of_S%26P_500_companies"
    companyWikipediaPages = []

    # get request
    response = requests.get(tableUrl)

    # create soup object
    soup = BeautifulSoup(response.text, 'html.parser')

    # parse html 
    tableHtml=soup.find("table",{"id":"constituents"})
    base_url = "https://en.wikipedia.org"
    for tr in tableHtml.find_all('tr'):
        rowCounter = 0
        for a in tr.find_all('a'):
            rowCounter += 1
            if a.has_attr('title') and rowCounter == 2:
                complete_url = base_url + a.get("href")
                companyWikipediaPages.append(complete_url)
    print("Finished scraping company urls from Wikipedia...")
    print()
    return companyWikipediaPages[1:] # first entry is incorrect

def getCompanyTextDescription(df):
    print("Scraping text descriptions from Wikipedia...")
    globalCounter = 0
    company_description_list = []
    for url in df["Wikipedia_Url"]:
        response = requests.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
  
        text = ''
        counter = 0

        for p in soup.find_all('p'):
            text += p.text
            if counter == 1:
                break
            counter += 1
        # print(text)
        company_description_list.append(text[:-1])
        globalCounter += 1
        if globalCounter % 50 == 0:
            total_records = df.shape[0]
            print(f"{round(globalCounter/total_records, 2)*100} % Complete")
    print("Finished scraping text descriptions from Wikipedia...")
    print()
    return company_description_list

def createOrganizedDictionary(df):
    companies = {}
    sortedCipherDict = dict(sorted(createCompanySymbolCipher().items())) 
    print(sortedCipherDict.keys())
    for index, row in df.iterrows():
        company = {}
        stockSymbol = row["Symbol"]

        company["Sector"] = row["GICS Sector"]
        company["Location"] = row["Headquarters Location"]
        company["Founded"] = row["Founded"]
        company["Wikipedia_Url"] = row ["Wikipedia_Url"]
        company["Company_Description"] = row["Company_Description"]
        company["Name"] = sortedCipherDict[stockSymbol]

        # store one company in larger company dictionary
        companies[stockSymbol] = company

    return companies

if __name__ == '__main__':
    # create intial df
    sp_assets = pd.read_html('https://en.wikipedia.org/wiki/List_of_S%26P_500_companies')
    spx500_df = sp_assets[0][["Symbol", "Security", "GICS Sector", "Headquarters Location", "Founded"]]

    # find index
    idx1 = spx500_df[spx500_df["Symbol"] == "BF.B"].index
    idx2 = spx500_df[spx500_df["Symbol"] == "BRK.B"].index

    # update tickers that are not compatiable with yfinance BF-B & BRK-B
    spx500_df.loc[idx1, 'Symbol'] = 'BF-B'
    spx500_df.loc[idx2, 'Symbol'] = 'BRK-B'
    print(spx500_df[spx500_df["Symbol"] == "BF-B"])
    print(spx500_df[spx500_df["Symbol"] == "BRK-B"])
    print(spx500_df[spx500_df["Symbol"] == "BG"])

    # # add a symbol column
    # spx500_df.insert(spx500_df.shape[1], "Symbol", list(createCompanySymbolCipher().keys()))
    # add a URL column
    wikipediaUrls = getCompanyUrlFromWikipedia()
    spx500_df.insert(spx500_df.shape[1], "Wikipedia_Url", wikipediaUrls)

    # add a Company Description column
    descriptions = getCompanyTextDescription(spx500_df)
    spx500_df.insert(spx500_df.shape[1], "Company_Description", descriptions)

    # sort the df by symbol
    spx500_df = spx500_df.sort_values("Symbol")

    companiesDictionary= createOrganizedDictionary(spx500_df)

    with open('api_data/company.json', 'w') as fp:
        # print(companyDescriptionDictionary, indent=4)
        json.dump(companiesDictionary, fp, indent=4, default=str)
        print("Saved dictionary into a Json")