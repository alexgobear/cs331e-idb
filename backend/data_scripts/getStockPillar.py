import yfinance as yf
import requests
import json
from getCompanyPillar import createCompanySymbolCipher
from os import path
from tqdm import tqdm

def getStockData():
    print("Getting Stock information from yfinance...")
    companies = {}
    spx500Tickers = list(createCompanySymbolCipher().keys())
    print(spx500Tickers) # list tickers to get a visual of the formatting

    # get features for each stock and store into a higher-level dictionary
    for idx, ticker in enumerate(spx500Tickers):
        print(ticker)
        temp_dictionary = {}
        company = yf.Ticker(ticker)

        # get historical market data - open, high, low, close, etc..
        hist = company.history(period="100y")

        # store featureees into a temporary dictionary
        temp_dictionary["date"] = list(hist.index)
        temp_dictionary["closingPrice"] = list(hist.Close)
        holders = company.institutional_holders["Holder"]
        if holders is None:
            temp_dictionary["First_Institutional_Holder"] = "N/A"
            temp_dictionary["Second_Institutional_Holder"] = "N/A"
            temp_dictionary["Third_Institutional_Holder"] = "N/A"
        else:
            temp_dictionary["First_Institutional_Holder"] = holders[0]
            temp_dictionary["Second_Institutional_Holder"] = holders[1]
            temp_dictionary["Third_Institutional_Holder"] = holders[2]

        # store
        companies[ticker] = temp_dictionary

        # keep track of results
        if idx % 10 == 0:
            print(f"{round((idx/len(spx500Tickers))*100,2)}% Complete")

    print("Done.")
        
    return companies

if __name__ == '__main__':
    stockDataDictionary = getStockData()
    # print(json.dumps(company_dictionary, indent=4))
    with open('api_data/stock.json', 'w') as fp:
        json.dump(stockDataDictionary, fp, indent=4, default=str)
        print("Saved dictionary into a Json")