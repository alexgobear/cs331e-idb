import requests
import json

root_url = 'https://gitlab.com/api/v4/projects/42666053'
gitlab_ids = {'Ryan': 12421371, 'Alex': 12344881, 'Zach': 12365152, 'Nathan': 9033082, 'Donia': 9584152}

def getCommits():
    url = root_url + '/repository/commits'
    params = {"per_page": 100, "page": 1}
    commit_tracker = {'Ryan': 0, 'Alex': 0, 'Zach': 0, 'Nathan': 0, 'Donia': 0}
    commits = []

    while True:
        response = requests.get(url, params=params)
        page_commits = response.json()

        if page_commits == []:
            break

        commits.extend(page_commits)
        params["page"] += 1
    
    for commit in commits:
        # split first and last name to just first
        author = commit["author_name"].split()[0]

        # correct for Zach's name
        if author == "Zachary":
            author = "Zach"
        elif author == "dlmobs":
            author = "Donia"
        
        commit_tracker[author] += 1
    
    return commit_tracker

def getIssues():
    url = root_url + '/issues_statistics?author_id='
    issue_tracker = {'Ryan': 0, 'Alex': 0, 'Zach': 0, 'Nathan': 0, 'Donia': 0}

    for key, ID in gitlab_ids.items():
        url_temp = url + str(ID)
        response = requests.get(url_temp)
        data = json.loads(response.content)

        issue_tracker[key] = data["statistics"]["counts"]["all"]

    return issue_tracker


# print(getCommits())
# print(getIssues())