from flask import jsonify, request, render_template
from populateDatabase import app, db, Company, Stock, Stock_Date_Price, Lawsuit, Stock_Holders, populate, BaseModel
from gitlabStats import getCommits, getIssues
from sqlalchemy import text, or_, types, String
from flask_cors import CORS

# test models
from subprocess import run, PIPE, STDOUT

CORS(app)

@app.route('/populate')
def populateDB():
    populate()
    return 'database populated'


@app.route('/test')
def testDB():

    # https://docs.python.org/3.6/library/subprocess.html
    # https://coverage.readthedocs.io/en/6.2/cmd.html#cmd-report
    args = (
        ('python', '-m', 'coverage', 'run', '--branch', 'test_models.py'),
        ('python', '-m', 'coverage', 'html', '-d', 'templates'), # make flask think it's a template
        ('rm', 'templates/.gitignore')
    )

    for arg in args:
        run(arg, stdout=PIPE, stderr=STDOUT, encoding='utf-8', shell=True)

    return render_template('index.html')


@app.route('/')
def splash():
    return 'api splash page'


@app.route('/companies')
def companies():
    companies = db.session.query(Company).all()
    return jsonify({company.name: {**company.as_dict(), **getLatestPrice(company), **getLawsuitCount(company)} for company in companies})


@app.route('/lawsuits')
def lawsuits():
    lawsuits = db.session.query(Lawsuit).all()

    data = {}
    for row in lawsuits:
        # Create a dictionary of each Lawsuit object with its id as the key
        lawsuit_dict = {row.id: row.as_dict()}

        # Append the lawsuit_dict to the list associated with the company_symbol key
        if row.company_symbol not in data:
            data[row.company_symbol] = []

        data[row.company_symbol].append(lawsuit_dict)

    return jsonify(data)


@app.route('/stocks')
def stocks():
    stocks = db.session.query(Stock).all()
    return jsonify({stock.symbol: stock.as_dict() for stock in stocks})


@app.route('/stock_dp')
def stock_date_prices():
    stock_date_prices = db.session.query(Stock_Date_Price).all()
    return jsonify({stock_dp.symbol: stock_dp.as_dict() for stock_dp in stock_date_prices})


@app.route('/stock_holders')
def stock_holders():
    stock_holders = db.session.query(Stock_Holders).all()
    return jsonify({holder.symbol: holder.as_dict() for holder in stock_holders})


# routes for singleCompany data
@app.route('/companies/<ticker>')
def oneCompanyCompanyInfo(ticker):
    # Construct a raw SQL statement using the `text` function
    sql_statement = text('''
        SELECT 
            *
        FROM 
            companies
        WHERE 
            symbol=:symbol  -- company ticker
    ''')

    # Create a `Query` object using `from_statement`
    c = db.session.query(Company).from_statement(sql_statement)

    # Pass parameters to the query using the `params` method
    results = c.params(symbol=ticker).all()

    return jsonify([row.as_dict() for row in results])


@app.route('/lawsuits/<ticker>')
def oneCompanyLawsuitInfo(ticker, limit=1000000):
    # Construct a raw SQL statement using the `text` function
    sql_statement = text(f'''
        SELECT 
            *
        FROM 
            lawsuits
        WHERE 
            company_symbol=:company_symbol  -- company ticker
        LIMIT {limit}
    ''')

    # Create a `Query` object using `from_statement`
    l = db.session.query(Lawsuit).from_statement(sql_statement)

    # Pass parameters to the query using the `params` method
    results = l.params(company_symbol=ticker).all()

    return jsonify({ticker: [row.as_dict() for row in results]})


@app.route('/stockPrice/<ticker>')
def oneCompanyStockPriceInfo(ticker):
    sql_statement = text("""
        SELECT
            *
        FROM 
            stock_date_prices
        WHERE
            symbol=:symbol
        """
    )

    # Create a `Query` object using `from_statement`
    s = db.session.query(Stock_Date_Price).from_statement(sql_statement)

    # Pass parameters to the query using the `params` method
    results = s.params(symbol=ticker).all()

    return jsonify([row.as_dict() for row in results])


@app.route('/stocks/<ticker>')
def oneCompanyStockInfo(ticker):
    # Construct a raw SQL statement using the `text` function
    sql_statement = text('''
        SELECT 
            *
        FROM 
            stocks
        WHERE 
            symbol=:symbol  -- company ticker
    ''')

    # Create a `Query` object using `from_statement`
    q = db.session.query(Stock).from_statement(sql_statement)

    # Pass parameters to the query using the `params` method
    results = q.params(symbol=ticker).all()

    return jsonify([row.as_dict() for row in results])


@app.route('/stock_holders/<ticker>')
def oneCompanyStockHolderInfo(ticker):
    # Construct a raw SQL statement using the `text` function
    sql_statement = text('''
        SELECT 
            *
        FROM 
            stock_holders
        WHERE 
            symbol=:symbol  -- company ticker
    ''')

    # Create a `Query` object using `from_statement`
    q = db.session.query(Stock_Holders).from_statement(sql_statement)

    # Pass parameters to the query using the `params` method
    results = q.params(symbol=ticker).all()

    return jsonify([row.as_dict() for row in results])


@app.route('/stock_dp/<ticker>')
def oneCompanyStockDateAndPriceInfo(ticker):
    # Construct a raw SQL statement using the `text` function
    sql_statement = text('''
        SELECT 
            *
        FROM 
            stock_date_prices
        WHERE 
            symbol=:symbol  -- company ticker
    ''')

    # Create a `Query` object using `from_statement`
    q = db.session.query(Stock_Date_Price).from_statement(sql_statement)

    # Pass parameters to the query using the `params` method
    results = q.params(symbol=ticker).all()

    return jsonify([row.as_dict() for row in results])


@app.route('/stats')
def stats():
    team = getTeam()

    # update commits and issues
    all_commits = getCommits()
    all_issues = getIssues()

    total_commits = 0
    total_issues = 0

    for person in team:
        full_name = person["name"]

        if full_name == 'Total':
            continue

        first_name = full_name.split()[0]

        total_commits += all_commits[first_name]
        total_issues += all_issues[first_name]

        person["commits"] = str(all_commits[first_name])
        person["issues"] = str(all_issues[first_name])

    
    # stats = {"total_commits": str(total_commits), "total_issues": str(total_issues), "total_unittests": str(0)}

    return jsonify(team)

def getTeam():
    return [{'name': 'Ryan Mehendale',
          'photo': 'ryan.png',
           'bio': 'I am currently a mathematics student at the University of Texas, where I am honing my technical and problem-solving skills with a particular interest in scientific computing. In my free time, I enjoy woodworking and playing piano. I get a lot of fulfillment out of being in a team environment and working with others to find efficient and creative solutions to problems.',
           'responsibilities': 'Bootstrap, Flask, and Documentation',
           'commits': '...',
           'issues': '...',
           'tests': '0'},
          {'name': 'Alex Gobert',
          'photo': 'alex_crop.jpg',
           'bio': 'I\'m an aspiring Software Engineer from Houston, TX and the father to a Pembroke Welsh Corgi named Momo. I love my dog, cooking, and spending time with family. I work part-time at HEB and I\'ve held an internship with the Texas Department of Transportation. I\'m currently open to full-time positions as an entry-level software developer.',
           'responsibilities': 'Database, Backend, GCP, and Documentation',
           'commits': '...',
           'issues': '...',
           'tests': '5'},
          {'name': 'Donia Mobli',
          'photo': 'donia.jpg',
           'bio': 'I am a fifth-year Chemical Engineering major minoring in Persian and earning the Elements of Computing Certificate at the University of Texas at Austin. With my educational background, my interests lie in working as a software developer for a process engineering team. In my free time, I love to read, play video games on my custom built PC, and spend time with my friends, family, and dog, Ella.',
           'responsibilities': 'Full Stack',
           'commits': '...',
           'issues': '...',
           'tests': '0'},
          {'name': 'Nathan Hardham',
          'photo': 'nathan.jpg',
           'bio': 'I\'m a Chemical Engineering senior at the University of Texas at Austin. I have also completed the Elements of Computing Certificate and have worked as a Data Engineering intern at Bright Health. I\'m originally from Houston, TX but spent several years in Anchorage, Alaska where I picked up a passion for hiking, biking, and climbing. When not on campus or at the Austin Bouldering Project, I\'m probably watching a show, practicing the piano, or playing board games with friends.',
           'responsibilities': 'Frontend',
           'commits': '...',
           'issues': '...',
           'tests': '0'},
          {'name': 'Zach Moss',
          'photo': 'zach.jpeg',
           'bio': 'I\'m a fifth-year Neuroscience major attending the University of Texas at Austin with an interest in data and software. I have experience scraping data from the web and deriving conclusions from data using hypothesis testing. On a more personal note, I enjoy playing soccer, being outdoors, and hanging out with friends.',
           'responsibilities': 'Data Scraping, Database, and Backend',
           'commits': '...',
           'issues': '...',
           'tests': '10'}
          ]


def getLatestPrice(company: Company) -> dict:
    query = db.session.query(Stock_Date_Price)

    query = query.filter_by(symbol = company.symbol) # filter
    query = query.order_by(Stock_Date_Price.date.desc()) # sort descending
    query = query.limit(1)

    return query.first().as_dict()


def getLawsuitCount(company: Company) -> dict:
    query = db.session.query(Lawsuit.company_symbol)

    query = query.filter_by(company_symbol = company.symbol)
    
    return {'num_lawsuits': query.count()}

# populate()
if __name__ == '__main__':
    # populate()
    app.run(debug=True)
