// the following will perform two api calls, one with the provided local host url, the other with the domain api

const fetchData = async (apiLinks) => {
    for (const apiLink of apiLinks) {
        try {
            const response = await fetch(apiLink);
            if (response.ok) {
                return await response.json();
            }
        } catch (error) {
            console.error(`Failed to fetch data from ${apiLink}`, error)
        }
    }
    return {};
}


export default fetchData