import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

const Header = () => {
  return (
    <Navbar bg="light" expand="lg" style={{marginLeft: '20px'}}>
      <Navbar.Brand href="/">Classless Action</Navbar.Brand>
      <Navbar.Toggle aria-controls="navbarSupportedContent" />
      <Navbar.Collapse id="navbarSupportedContent">
        <Nav className="mr-auto">
          <Nav.Link href="/about">About</Nav.Link>
          <Nav.Link href="/companies">SPX500 Companies</Nav.Link>
          <Nav.Link href="/lawsuits">Class Action Lawsuits</Nav.Link>
          <Nav.Link href="/stocks">Stocks</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Header;