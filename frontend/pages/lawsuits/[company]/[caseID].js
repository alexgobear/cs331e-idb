import Navbar from '../../../components/navBar'
import Head from 'next/head'
import Link from 'next/link'
import {useState, useEffect} from 'react'

import Card from 'react-bootstrap/Card'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import company from '../../companies/[company]'

const local_companies_url = process.env.COMPANIES_LOCALHOST_URL
const local_lawsuits_url = process.env.LAWSUITS_LOCALHOST_URL

const api_companies_url = process.env.COMPANIES_API_URL
const api_lawsuits_url = process.env.LAWSUITS_API_URL


const Case = ({ company_data, companyName, caseID, lawsuit_data }) => {

    const companyDescription = company_data["description"].replace(/\[\d+\]/g, '');

    return (
        <>
        <Head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <title>Classless Action</title>
        </Head>
        <div>
            <Navbar></Navbar>
        </div>

        {/* this padding doesn't work for some reason */}
        <div styles={{paddingLeft: "20px", paddingRight: "20px"}}>

            <div className="container text-center py-5">
                <h1 className="display-4">{companyName}</h1>
            </div>
            <div className="row px-5" style={{display: "center", textAlign: "center"}}>
                <p style={{textAlign: "center"}}>{companyDescription}</p> 
            </div>
            <br></br>

            <Row className="row px-5">
                <Col md={8}>
                    <Card style={{backgroundColor: "#D3D3D3"}}>
                        <Card.Body>
                            <Card.Title>
                                <h1 className="display-6">{lawsuit_data.name}</h1>
                            </Card.Title>
                            <br></br>
                            <Card.Text className="font-weight-bold">Decision Data:</Card.Text>
                            <Card.Text>{lawsuit_data["decision_date"]}</Card.Text>
                            <Card.Text className="font-weight-bold">Court Name:</Card.Text>
                            <Card.Text>{lawsuit_data["court_name"]}</Card.Text>
                            <Card.Text className="font-weight-bold">Jurisdiction:</Card.Text>
                            <Card.Text>{lawsuit_data["jurisdiction"]}</Card.Text>
                            <Button variant="primary">
                                <Link href={lawsuit_data["pdf"]} style={{ color: 'white', textDecoration: 'none' }}>Case File</Link>
                            </Button>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md={4}>
                    <Container className="justify-content-between">
                    <Card style={{backgroundColor: "#D3D3D3"}}>
                        <Card.Body>
                            <Container className="d-flex align-items-center justify-content-center">
                                    <Button variant="primary">
                                    <Link href={"/companies/" + companyName} style={{ color: 'white', textDecoration: 'none' }}>Company Information</Link>
                                    </Button>
                            </Container>
                        </Card.Body>
                    </Card>
                    <Card style={{backgroundColor: "#D3D3D3"}}>
                        <Card.Body>
                            <Container className="d-flex align-items-center justify-content-center">
                                <Button variant="primary">
                                    <Link href={"/stocks/" + company_data["symbol"]} style={{ color: 'white', textDecoration: 'none' }}>Stock Information</Link>
                                </Button>
                            </Container>
                        </Card.Body>
                    </Card>
                    </Container>
                </Col>
            </Row>
        </div>
        </>
    )
}

export async function getServerSideProps(context) {
    
    const { params } = context
    const companyName = params.company
    const caseID = params.caseID

    // loop through links to make requests
    let res_companies = await fetch(api_companies_url);
    if (!res_companies.ok) {
        // backend request failed, try localhost request
        res_companies = await fetch(local_companies_url);
        if (!res_companies.ok) {
            return {
                props: { data: {}}
            }
        }
    }

    let res_lawsuits = await fetch(api_lawsuits_url);
    if (!res_lawsuits.ok) {
        // backend request failed, try localhost request
        res_lawsuits = await fetch(local_lawsuits_url);
        if (!res_lawsuits.ok) {
            return {
                props: { data: {}}
            }
        }
    }

    const companies_data = await res_companies.json();
    const lawsuits_data = await res_lawsuits.json();
    
    const company_data = companies_data[companyName]
    const ticker = company_data["symbol"]
    company_data["lawsuits"] = lawsuits_data[ticker]

    var lawsuit_data = {}
    for (const lawsuit of company_data["lawsuits"])
        if (caseID in lawsuit) {
            lawsuit_data = lawsuit[caseID]
        }
    
        
    return {
        props: { 
            company_data : company_data,
            companyName : companyName,
            caseID : caseID,
            lawsuit_data: lawsuit_data
        }
    }


};

export default Case