import React from 'react'
import { useState } from 'react'
import Navbar from '../../components/navBar'
import fetchData from '../../components/apiCall'
import Head from 'next/head'
import Link from 'next/link'
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'
import InputGroup from 'react-bootstrap/InputGroup'
import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import ToggleButton from 'react-bootstrap/ToggleButton'

const local_companies_url = process.env.COMPANIES_LOCALHOST_URL
const local_lawsuits_url = process.env.LAWSUITS_LOCALHOST_URL

const api_companies_url = process.env.COMPANIES_API_URL
const api_lawsuits_url = process.env.LAWSUITS_API_URL

const links = {"companies_urls": [api_companies_url, local_companies_url], "lawsuits_urls": [api_lawsuits_url, local_lawsuits_url]}


const Lawsuits = ({companies_data, lawsuits_data}) => {
    const colTitles = ['Company', 'Stock Ticker', 'Class Action Lawsuits']

    // Define state for the selected option and filter keyword
    const [sortBy, setSortBy] = useState("companyName");
    const [searchQuery, setSearchQuery] = useState("");

    const handleSort = (sortKey) => {
      setSortBy(sortKey);
    };

    const handleSearchChange = (e) => {
      setSearchQuery(e.target.value);
    };

  
    const filteredAndSortedCompaniesData = Object.entries(companies_data)
      .filter((company) => {
        if (!searchQuery) {
          return true;
        }

        const ticker = company[1]["symbol"]
        const companyNameMatch = company[0].toLowerCase().includes(searchQuery.toLowerCase());
        const stockTickerMatch = ticker.toLowerCase().includes(searchQuery.toLowerCase());
        // const caseIDMatch = lawsuits_data[ticker].hasOwnProperty(parseInt(searchQuery, 10))

        return (searchQuery && (companyNameMatch || stockTickerMatch))
      })

      .sort((a, b) => {
        const companyA = a;
        const companyB = b;
        
        if (sortBy === "companyName") {
            return companyA[0].localeCompare(companyB[0]);
        } else if (sortBy === "stockTicker") {
            return companyA[1]["symbol"].localeCompare(companyB[1]["symbol"]);
        } else if (sortBy === "classActionLawsuits") {
            if (companyA[1]["num_lawsuits"] > companyB[1]["num_lawsuits"]) {
              return -1
            } else if (companyA[1]["num_lawsuits"] < companyB[1]["num_lawsuits"]) {
              return 1
            } else {
              return 0
            }
        } else {
            // Default sorting by company name
            return companyA[0].localeCompare(companyB[0]);
        }
      })
      const [radioValue, setRadioValue] = useState('1');
  
    const radios = [
      { name: 'Company Name', value: '1', sortKey: 'companyName' },
      { name: 'Stock Ticker', value: '2', sortKey: 'stockTicker' },
      { name: 'Number of Class Action Lawsuits', value: '3', sortKey: 'classActionLawsuits' },
    ];
  
      return (
        <>
            <Head>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <title>Classless Action</title>
            </Head>
  
            <div>
                <Navbar></Navbar>
                <div class="container pt-5 px-5 pb-3">
                  <Card>
                    <div class="row">
                      <div class="align-items-center justify-content-center">
                        {/* <input type="text" value={searchQuery} onChange={handleSearchChange} placeholder="Search" /> */}
                        <div class="container pt-3 d-flex align-items-center justify-content-center">
                          <h3>
                            Sort By
                          </h3>
                        </div>
                        <div className="container pt-3 text-center">
                          <div className="row">
                            <div className="col">
                              <div className="px-3">
                                <ButtonGroup>
                                  {radios.map((radio, idx) => (
                                    <ToggleButton
                                      key={idx}
                                      id={`radio-${idx}`}
                                      type="radio"
                                      variant={'outline-primary'}
                                      name="radio"
                                      value={radio.value}
                                      checked={radioValue === radio.value}
                                      onClick={() => handleSort(radio.sortKey)}
                                      onChange={(e) => setRadioValue(e.currentTarget.value)}
                                    >
                                      {radio.name}
                                    </ToggleButton>
                                  ))}
                                </ButtonGroup>
                              </div>
                            </div>
                          </div>
                        </div>
                        </div>
                      <div class="pb-3">
                        <div class="container pt-3 d-flex align-items-center justify-content-center">
                          <InputGroup className="my-2">
                            <InputGroup.Text>Search:</InputGroup.Text>
                            <Form.Control
                              type="text"
                              value={searchQuery}
                              onChange={handleSearchChange}
                              placeholder="Search for a Company or Stock Ticker"
                            />
                          </InputGroup>
                        </div>
                      </div>
                    </div>
                  </Card>
                </div>

                <div id='companyTable'>
                    <Container>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                {colTitles.map(title => {
                                    return (<th>{title}</th>)
                                })}
                            </tr>
                        </thead>
                        <tbody>
                            {filteredAndSortedCompaniesData.map(company_data => {
                                const companyName = company_data[0]
                                const companyDict = company_data[1]
                                var company_lawsuits = []
                                if (companyDict["num_lawsuits"] === 0) {
                                    company_lawsuits = []
                                } else {
                                    company_lawsuits = lawsuits_data[companyDict["symbol"]]
                                }

                                return (
                                    <tr>
                                        <td>
                                            <Link className="nav-link" style={{ textDecoration: 'none', color: 'blue' }} href={"companies/" + companyName}>{companyName}</Link>
                                        </td>
                                        <td>
                                            <Link className="nav-link" style={{ textDecoration: 'none', color: 'blue' }} href={"stocks/" + companyDict.symbol}>{companyDict.symbol}</Link>
                                        </td>
                                        <td>
                                            <div className="dropdown">
                                                <button
                                                    className="btn btn-secondary dropdown-toggle"
                                                    type="button"
                                                    id={`dropdownMenuButton-${companyName}`}
                                                    data-bs-toggle="dropdown"
                                                    aria-haspopup="true"
                                                    aria-expanded="false"
                                                >{companyDict.num_lawsuits} Class Action Lawsuits
                                                </button>
                                                <div
                                                    className="dropdown-menu"
                                                    aria-labelledby={`dropdownMenuButton-${companyName}`}
                                                    style={{ maxHeight: '200px', overflowY: 'auto' }}
                                                >
                                                    {company_lawsuits.map((lawsuit) => {
                                                        const caseID = Object.keys(lawsuit)[0]
                                                        return (
                                                            <a className="dropdown-item" href={`lawsuits/${companyName}/${caseID}`}>
                                                            {lawsuit[caseID]["name"].substring(0, 40)}
                                                            </a>
                                                        );
                                                    })}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </Table>
                    </Container>
                </div>
            </div>
        </>
    )
}

export async function getServerSideProps() {
    const data_calls = {}

    // loop through links to make requests
    for (let key in links) {
      // get link pairs, 0 is localhost, 1 is api
      const value = links[key]

      // try fetching data from the links
      const data = await fetchData(value);
      data_calls[key] = data;
    } 

    const companies_data = data_calls["companies_urls"]
    const lawsuits_data = data_calls["lawsuits_urls"]

    return {
        props: { 
            companies_data : companies_data,
            lawsuits_data : lawsuits_data
        }
    }

};


export default Lawsuits