import Navbar from '../components/navBar'
import fetchData from '../components/apiCall'
import React, { useState } from 'react';
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Row from 'react-bootstrap/Row'
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import Collapse from 'react-bootstrap/Collapse'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Container from 'react-bootstrap/Container'

import Link from 'next/link'

const api_url = process.env.STATS_API_URL
const localhost_url = process.env.STATS_LOCALHOST_URL
const coverage_api_url = process.env.COVERAGE_API_URL
const coverage_localhost_url = process.env.COVERAGE_LOCALHOST_URL

const links = {"stats_urls": [api_url, localhost_url], "coverage_urls": [coverage_api_url, coverage_localhost_url]}


const About = ({ stats_data , totals, coverage_url}) => {
    const [open1, setOpen1] = useState(true);
    const [open2, setOpen2] = useState(false);

    const handleClick1 = () => {
        setOpen1(!open1);
        if (open2) {
            setOpen2(false)
        }
    };

    const handleClick2 = () => {
        setOpen2(!open2);
        if (open1) {
            setOpen1(false)
        }
      };


    return (
    <>
        <div>
            <Navbar></Navbar>

            <div className="container text-center py-5">
                <h1 className="display-4">About</h1>
            </div>
            <div className="container text-center py-3">
                <h3 className="display-6">Team Members</h3>
            </div>

        </div>

        {/* people cards */}

        <Container fluid style={{height: "calc(100vh - 200px)"}}>
        <div className="row justify-content-center">
            <div className="card-container d-flex flex-wrap justify-content-center">
                {stats_data.map(person => {
                    return (
                        <div key={person.name} className="card m-2" style={{width: "15%", minWidth: "16rem"}}>
                            <img src={`/images/members/${person.photo}`} alt="Card image cap" className="card-img-top img-fluid"/>
                            <div className="card-body" style={{ height: '465px' }}>
                            <h5 className="card-title" style={{textAlign: "center"}}>{ person.name }</h5>
                            <p className="card-text">{ person.bio }</p>
                            </div>
                            <ul className="list-group list-group-flush" style={{ height: '300px' }}>
                            <li className="list-group-item">
                                <div style={{ height: '75px' }}>
                                    <h6>Responsibilities</h6>{ person.responsibilities }
                                </div>
                            </li>
                            <li className="list-group-item">
                                <h6>Number of Commits</h6>{ person.commits }
                            </li>
                            <li className="list-group-item">
                                <h6>Number of Issues</h6>{ person.issues }
                            </li>
                            <li className="list-group-item">
                                <h6>Number of Unit Tests</h6>{ person.tests}
                            </li>
                            </ul>
                        </div>
                    );
                })}
            </div>
        </div>

        <div className="container text-center py-5">
            <h3 className="display-6">Resources</h3>
        </div>

        <div className="row px-5">
        {/*gitlab card*/}
            <div className="col-6 justify-content-center">
                <div className="card">
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item">
                            <div className="row">
                                <div className="col">
                                    <b>Total Number of Commits</b><br/>{ totals.total_commits }
                                </div>
                                <div className="col">
                                    <b>Total Number of Issues</b><br/>{ totals.total_issues }
                                </div>
                                <div className="col">
                                    <b>Total Number of Unit Tests</b><br/>{ totals.total_unittests }
                                </div>
                            </div>
                        </li>
                        <li className="list-group-item text-center">
                            <Row>
                                <img className="mx-auto d-block img-fluid" style={{width: "15rem"}} alt="avatar1" src="/images/misc/gitlab.png" />
                                <h3 className="display-6">GitLab</h3>
                            </Row>
                        </li>
                        <li className="list-group-item">
                            <div className="row align-items-center">
                                <div className="col d-flex justify-content-center">
                                    <a href="https://gitlab.com/alexgobear/cs331e-idb" className="btn btn-secondary btn-lg active" role="button"
                                        aria-pressed="true">Repo</a>
                                </div>
                                <div className="col d-flex justify-content-center">
                                    <a href="https://gitlab.com/alexgobear/cs331e-idb/-/wikis/home" className="btn btn-secondary btn-lg active"
                                        role="button" aria-pressed="true">Wiki</a>
                                </div>
                                <div className="col d-flex justify-content-center">
                                    <a href="https://gitlab.com/alexgobear/cs331e-idb/-/issues" className="btn btn-secondary btn-lg active"
                                        role="button" aria-pressed="true">Issues</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <Card className="mt-4 align-items-center">
                    <h3 className="display-6">Unit test Run</h3>
                    <img className="mx-auto d-block img-fluid" style={{width: "18rem"}} alt="avatar1" src="/images/misc/unittest_run.png" />
                    <hr/>

                    <ButtonGroup>
                        <Link href={coverage_url}>
                            <Button>Database Coverage</Button> 
                        </Link>
                        <Link href="https://speakerdeck.com/mryan00/sweii-presentation">
                            <Button className="ml-2">Project Presentation</Button>
                        </Link>
                    </ButtonGroup>
                    <hr/>
                </Card>
            </div>

            {/*resource accordion*/}
            <div className="col-6 justify-content-center">
                <Accordion defaultActiveKey="0">
                    <Accordion.Item eventKey="0">
                        <Accordion.Header><h3>Data Sources</h3></Accordion.Header>
                        <Accordion.Body>
                            <h4 className="pt-3"><a href="https://en.wikipedia.org/wiki/List_of_S%26P_500_companies">Wikipedia</a> & <a href="https://en.wikipedia.org/w/api.php">Wikipedia API</a></h4>
                            <h6>SPX 500 companies and their descriptions</h6>
                            <hr/> 
                            <h4 className="pt-3"><a href="https://api.case.law/v1/">Caselaw Access Project</a></h4>
                            <h6>Class action lawsuit information for each SPX 500 company</h6>
                            <hr/>
                            <h4 className="pt-3"><a href="https://pypi.org/project/yfinance/">Yahoo! Finance scraped using yfinance</a></h4>
                            <h6>Stock data about each SPX 500 company</h6>
                        </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item eventKey="1">
                        <Accordion.Header><h3>Tools Used</h3></Accordion.Header>
                        <Accordion.Body>
                            <h4 className="pt-3"><a href="https://flask.palletsprojects.com/en/2.2.x/">Flask 2.2.2</a></h4>
                            <blockquote className="blockquote">
                                <h6>Flask is a micro web framework written in Python. It is classified as a microframework because it does not require
                                    particular tools or libraries. It has no database abstraction layer, form validation, or any other components where
                                    pre-existing third-party libraries provide common functions. However, Flask supports extensions that can add
                                    application features as if they were implemented in Flask itself. Extensions exist for object-relational mappers, form
                                    validation, upload handling, various open authentication technologies and several common framework related tools.
                                </h6>
                            </blockquote>
                            <figcaption className="blockquote-footer">
                                Wikipedia page for <cite title="Source Title">Flask (web framework)</cite>
                            </figcaption>
                            <hr/>
                            <h4 className="pt-3"><a href="https://nodejs.org/en">Node.js 16.13.0</a></h4>
                            <blockquote className="blockquote">
                                <h6>Node.js is a cross-platform, open-source server environment that can run on Windows, Linux, Unix, macOS, and more. 
                                    Node.js is a back-end JavaScript runtime environment, runs on the V8 JavaScript Engine, and executes JavaScript code 
                                    outside a web browser.
                                </h6>
                            </blockquote>
                            <figcaption className="blockquote-footer">
                                Wikipedia page for <cite title="Source Title">Node.js</cite>
                            </figcaption>
                            <hr/>
                            <h4 className="pt-3"><a href="https://react.dev/">React.js 18.2.0</a></h4>
                            <blockquote className="blockquote">
                                <h6>React is a free and open-source front-end JavaScript library for building user interfaces based on components.
                                </h6>
                            </blockquote>
                            <figcaption className="blockquote-footer">
                                Wikipedia page for <cite title="Source Title">React (software)</cite>
                            </figcaption>
                            <hr/>
                            <h4 className="pt-3"><a href="https://nextjs.org/">Next.js 13.2.4</a></h4>
                            <blockquote className="blockquote">
                                <h6>Used by some of the world's largest companies, Next.js enables you to create full-stack Web applications by extending 
                                    the latest React features, and integrating powerful Rust-based JavaScript tooling for the fastest builds.
                                </h6>
                            </blockquote>
                            <figcaption className="blockquote-footer">
                                Home Page of <cite title="Source Title">nextjs.org</cite>
                            </figcaption>
                            <hr/>
                            <h4 className="pt-3"><a href="https://react-bootstrap.github.io/">React-Bootstrap 2.7.2</a></h4>
                            <blockquote className="blockquote">
                                <h6>React-Bootstrap replaces the Bootstrap JavaScript. Each component has been built from scratch as a true React
                                     component, without unneeded dependencies like jQuery.</h6>
                            </blockquote>
                            <figcaption className="blockquote-footer">
                                Home Page of <cite title="Source Title">react-bootstrap.github.io</cite>
                            </figcaption>
                            <hr/>


                            <h4 className="pt-3"><a href="https://www.postgresql.org/">PostgreSQL 15.2</a></h4>
                            <blockquote className="blockquote">
                                <h6>PostgreSQL is a powerful, open source object-relational database system with over 35 years of active development that 
                                    has earned it a strong reputation for reliability, feature robustness, and performance.
                                </h6>
                            </blockquote>
                            <figcaption className="blockquote-footer">
                                Home Page of <cite title="Source Title">postgresql.org</cite>
                            </figcaption>
                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>
            </div>
        </div>
        </Container>
        
    </>
    )
}

const getWorkingURL = async (urls) => {
    for (const url of urls) {
      try {
        const response = await fetch(url);
        if (response.ok) {
          return url;
        }
      } catch (error) {
        console.error(`Failed to fetch data from ${url}`, error);
      }
    }
    return null;
}


export async function getServerSideProps() {
    const data_calls = {}
    const coverage_url = []

    // loop through links to make requests
    for (let key in links) {
        // get link pairs, 0 is localhost, 1 is api
        const value = links[key]

        if (key === "coverage_urls") {
            const workingURL = await getWorkingURL(links[key]);
            if (workingURL) {
                coverage_url.push(workingURL);
            }
        }
  
        // try fetching data from the links
        const data = await fetchData(value);
        data_calls[key] = data;
      } 
    
    // get totals
    let total_commits = 0
    let total_issues = 0

    for (const person_data of data_calls["stats_urls"]) {
        total_commits += parseInt(person_data.commits)
        total_issues += parseInt(person_data.issues)
    }

    const totals = {"total_commits": total_commits, "total_issues": total_issues, "total_unittests": 15}

    return {
        props: { stats_data : data_calls["stats_urls"] , totals: totals, coverage_url: coverage_url[0]}
    }

};



export default About