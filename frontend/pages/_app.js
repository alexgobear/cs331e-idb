// import '@/styles/index.module.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { SSRProvider } from '@react-aria/ssr';
// import dotenv from 'dotenv'

// dotenv.config()

// import 'bootstrap/dist/js/bootstrap.min.js';
// import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css'

// import 'datatables.net-dt/css/jquery.dataTables.css';
// import 'datatables.net-dt/js/dataTables.dataTables.min';


if (typeof window !== 'undefined') {
    require('bootstrap/dist/js/bootstrap.min.js');
}


export default function App({ Component, pageProps }) {
  return (
    <SSRProvider>
      <Component {...pageProps} />
    </SSRProvider>
  )
}