import Navbar from '../../components/navBar'
import fetchData from '../../components/apiCall'
import styles from '../../styles/companies.module.css'

import React, { useEffect, useRef, useState } from 'react';
import Head from 'next/head'
import Link from 'next/link'

import Container from 'react-bootstrap/Container'
import Card from 'react-bootstrap/Card'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import InputGroup from 'react-bootstrap/InputGroup'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import ToggleButton from 'react-bootstrap/ToggleButton'


const companies_local = process.env.COMPANIES_LOCALHOST_URL
const companies_api = process.env.COMPANIES_API_URL

const links = {"companies_urls": [companies_api, companies_local]}


const LazyLoadedRow = ({ rowData }) => {
  const [isVisible, setIsVisible] = useState(false);
  const rowRef = useRef();

  useEffect(() => {
    const observer = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            setIsVisible(true);
          }
        });
      },
      { threshold: 0.1 } // Adjust the threshold value based on your requirement
    );

    if (rowRef.current) {
      observer.observe(rowRef.current);
    }

    return () => {
      if (rowRef.current) {
        observer.unobserve(rowRef.current);
      }
    };
  }, [rowRef]);


  return isVisible ? (
    <Row className="align-items-center my-5">
      {rowData.map(company_data => {
          return (
              <Col className="col-lg-3 col-md-3 col-sm-3">
              <FlipCard company_data={company_data}/>
              </Col>
          )
      })}
    </Row> ) : (
    <div ref={rowRef} className="col-lg-3 col-md-3 col-sm-3"></div>
  )
};



const FlipCard = ({ company_data }) => {
  const [isFlipped, setIsFlipped] = useState(false);

  // const handleMouseEnter = () => {
  //   setIsFlipped(true);
  // };

  // const handleMouseLeave = () => {
  //   setIsFlipped(false);
  // };

  const handleClick = () => {
    setIsFlipped(!isFlipped);
  };

  const d_data = company_data[1]

  return (
    <Card style={{ 'height': '340px' }} className={`${styles['card-container']} ${isFlipped ? styles.flip : ''}`}>
      <div className={styles['card-face']}>
        <Card.Body className="d-flex flex-column">
          <Link href={"companies/" + company_data[0]} className="text-decoration-none">
            <Card.Img
              variant="top"
              className="rounded-image"
              style={{ border: '1px solid gray' }}
              width={200}
              height={250}
              src={'/images/companyLogos/' + d_data["symbol"] + '.png'}
            ></Card.Img>
          </Link>
        </Card.Body>

        <div style={{ position: 'absolute', bottom: '0', width: '100%'}}>
          <Card.Header className="d-flex align-items-center justify-content-center text-dark" style={{ 'height': '50px', 'width': '100%' }}>
            <div className="d-flex align-items-center justify-content-between" style={{ width: '100%' }}>
              <Card.Title>{company_data[0]}</Card.Title>
              <Card.Img
                src={'/images/misc/flipArrow.png'}
                // onMouseEnter={handleMouseEnter}
                // onMouseLeave={handleMouseLeave}
                onClick={handleClick}
                style={{ width: '25px', height: '25px' }}
              />
            </div>
          </Card.Header>
        </div>
      </div>

      {/* back of card */}
      <div className={`${styles['card-face']} ${styles['back']}`}>
        <Container className="pt-3">
          <p><strong>Founded:</strong> {d_data["founded"]} </p>
          <p><strong>Location:</strong> {d_data["location"]} </p>
          <Link href={"stocks/" + d_data["symbol"]} className="text-decoration-none">
            <p><strong>Stock Ticker:</strong> {d_data["symbol"]}</p>
          </Link>
          <Link href={"lawsuits/"} className="text-decoration-none">
            <p><strong>Class Action Lawsuits:</strong> {d_data["num_lawsuits"]} </p>
          </Link>
          <p><strong>Sector:</strong> {d_data["sector"]} </p>
          <div className="d-flex justify-content-between">
            <a href={"https://" + d_data["domain"]}>
              <Button variant="dark" size="sm" className="ml-auto">Company Website</Button>
            </a>
            <Link href={"companies/" + company_data[0]}>
              <Button variant="primary" size="sm">More Info</Button>
            </Link>
          </div>

        </Container>

        {/* add bottom with flip functionality */}
        <div style={{ position: 'absolute', bottom: '0', width: '100%'}}>
          <Card.Header className="d-flex align-items-center justify-content-center text-dark" style={{ 'height': '50px', 'width': '100%' }}>
            <div className="d-flex align-items-center justify-content-between" style={{ width: '100%' }}>
              <Card.Title>{company_data[0]}</Card.Title>
              <Card.Img
                src={'/images/misc/flipArrow.png'}
                // onMouseEnter={handleMouseEnter}
                // onMouseLeave={handleMouseLeave}
                onClick={handleClick}
                style={{ width: '25px', height: '25px' }}
              />
            </div>
          </Card.Header>
        </div>
      </div>
    </Card>
  )
}


const Companies = ( {companies_data} ) => {
    // Define state for the selected option and filter keyword
    const [sortBy, setSortBy] = useState("companyName");
    const [searchQuery, setSearchQuery] = useState("");

  
    const handleSort = (sortKey) => {
      setSortBy(sortKey);
    };

    const handleSearchChange = (e) => {
      setSearchQuery(e.target.value);
    };

  
    const filteredAndSortedCompaniesData = [...companies_data]
      .filter((company) => {
        if (!searchQuery) {
          return true;
        }

        const companyNameMatch = company[0].toLowerCase().includes(searchQuery.toLowerCase());
        const stockTickerMatch = company[1]["symbol"].toLowerCase().includes(searchQuery.toLowerCase());

        return (searchQuery && (companyNameMatch || stockTickerMatch))
      })

      .sort((a, b) => {
        const companyA = a;
        const companyB = b;
        
        if (sortBy === "companyName") {
            return companyA[0].localeCompare(companyB[0]);
        } else if (sortBy === "stockTicker") {
            return companyA[1]["symbol"].localeCompare(companyB[1]["symbol"]);
        } else if (sortBy === "classActionLawsuits") {
            if (companyA[1]["num_lawsuits"] > companyB[1]["num_lawsuits"]) {
              return -1
            } else if (companyA[1]["num_lawsuits"] < companyB[1]["num_lawsuits"]) {
              return 1
            } else {
              return 0
            }
        } else {
            // Default sorting by company name
            return companyA[0].localeCompare(companyB[0]);
        }
      })


    
    // reformatting to:
    // cc = [company_name, data_dict]
    // company_data = [[cc, cc, cc, cc], [cc, cc, cc, cc], ...]

    // convert companies dictionary to an array of key value pairs
    const formatted_companies_data = []

    // figure out length of filteredAndSortedCompaniesData
    const companies_count = filteredAndSortedCompaniesData.length
    const total_rows = Math.ceil(companies_count/4)
    var position = 0

    for (let i = 1; i <= total_rows; i++) {
        const temp_row = []

        while (temp_row.length !== 4 && filteredAndSortedCompaniesData[position]) {
          temp_row.push(filteredAndSortedCompaniesData[position])
          position += 1
        }
        formatted_companies_data.push(temp_row)
    }

    const [radioValue, setRadioValue] = useState('1');
  
    const radios = [
      { name: 'Company Name', value: '1', sortKey: 'companyName' },
      { name: 'Stock Ticker', value: '2', sortKey: 'stockTicker' },
      { name: 'Number of Class Action Lawsuits', value: '3', sortKey: 'classActionLawsuits' },
    ];

    return (
        <>
            <Head>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <title>Classless Action</title>
            </Head>
  
            <div>
                <Navbar></Navbar>
                <div class="container pt-5 px-5 pb-3">
                  <Card>
                    <div class="row">
                      <div class="align-items-center justify-content-center">
                        {/* <input type="text" value={searchQuery} onChange={handleSearchChange} placeholder="Search" /> */}
                        <div class="container pt-3 d-flex align-items-center justify-content-center">
                          <h3>
                            Sort By
                          </h3>
                        </div>
                        <div className="container pt-3 text-center">
                          <div className="row">
                            <div className="col">
                              <div className="px-3">
                                <ButtonGroup>
                                  {radios.map((radio, idx) => (
                                    <ToggleButton
                                      key={idx}
                                      id={`radio-${idx}`}
                                      type="radio"
                                      variant={'outline-primary'}
                                      name="radio"
                                      value={radio.value}
                                      checked={radioValue === radio.value}
                                      onClick={() => handleSort(radio.sortKey)}
                                      onChange={(e) => setRadioValue(e.currentTarget.value)}
                                    >
                                      {radio.name}
                                    </ToggleButton>
                                  ))}
                                </ButtonGroup>
                              </div>
                            </div>
                          </div>
                        </div>
                        </div>
                      <div class="pb-3">
                        <div class="container pt-3 d-flex align-items-center justify-content-center">
                          <InputGroup className="my-2">
                            <InputGroup.Text>Search:</InputGroup.Text>
                            <Form.Control
                              type="text"
                              value={searchQuery}
                              onChange={handleSearchChange}
                              placeholder="Search for a Company or Stock Ticker"
                            />
                          </InputGroup>
                        </div>
                      </div>
                    </div>
                  </Card>
                </div>

                <Container>
                  {formatted_companies_data.map(five_company_row => {
                    return (
                      <LazyLoadedRow rowData={five_company_row} />
                    )
                  })}
                </Container>
            </div>
        </>
      )
}


export async function getServerSideProps() {
    const data_calls = {}

    // loop through links to make requests
    for (let key in links) {
      // get link pairs, 0 is localhost, 1 is api
      const value = links[key]

      // try fetching data from the links
      const data = await fetchData(value);
      data_calls[key] = data;
    } 

    data_calls["companies_urls"] = Object.entries(data_calls["companies_urls"]);

    return {
        props: { 
            companies_data : data_calls["companies_urls"] , 
        }
    }

};


export default Companies
