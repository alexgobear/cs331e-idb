import Navbar from '../../components/navBar'
import fetchData from '../../components/apiCall'
import Head from 'next/head'
import Image from 'next/image'
import {useState, useEffect} from 'react'
import Card from 'react-bootstrap/Card'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Link from 'next/link'


const companies_local = process.env.COMPANIES_LOCALHOST_URL
const companies_api = process.env.COMPANIES_API_URL
const stocks_local = process.env.STOCKS_LOCALHOST_URL
const stocks_api = process.env.STOCKS_API_URL
const stocksSH_local = process.env.STOCKS_SH_LOCALHOST_URL
const stocksSH_api = process.env.STOCKS_SH_API_URL
const lawsuits_local = process.env.LAWSUITS_LOCALHOST_URL
const lawsuits_api = process.env.LAWSUITS_API_URL

const links = {"companies_urls": [companies_api, companies_local], "stocks_urls": [stocks_api, stocks_local],
                "stockssh_urls": [stocksSH_api, stocksSH_local], "lawsuits_urls": [lawsuits_api, lawsuits_local]}


const company = ({ company_data, company_stock_data, stockssh_data, companyName, lawsuits_data }) => {

  const [companyDescription, setCompanyDescription] = useState("");

  useEffect(() => {
        let updatedCompanyDescription = company_data["description"].replace(/ *\[[^)]*\] */g, "");
        const descriptionWords = updatedCompanyDescription.split(' ');
        if (descriptionWords[0] == companyName.split(' ')[0].slice(1)) {
          setCompanyDescription(company_data["description"].replace(/ *\[[^)]*\] */g, "").replace(companyName.split(' ')[0].slice(1), companyName.split(' ')[0]));
        }
        else {
          setCompanyDescription(company_data["description"].replace(/ *\[[^)]*\] */g, ""));
        }
  }, [company_data, companyName]);



  return (
    <>
      <Head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Classless Action</title>
      </Head>

      <div>
        <Navbar></Navbar>
        <Container className="mt-5">
          {/* company description/picture and links to other sites/extra info */}
          <Row className="pb-5">
            {/* company description and picture */}
            <Col md={4}>
              <Card className="mb-3">
                <Card.Img variant='top' src={'/images/companyLogos/' + company_data["symbol"] + '.png'}></Card.Img>
                <Card.Body>
                  <Card.Title className="text-center">
                    {companyName}
                  </Card.Title>
                </Card.Body>
              </Card>
              <Card>
                <Card.Body>
                  <h4>Company Information</h4>
                  <p><strong>Founded:</strong> {company_data["founded"]} </p>
                  <p><strong>Location:</strong> {company_data["location"]} </p>
                  <p><strong>Sector:</strong> {company_data["sector"]} </p>
                </Card.Body>
              </Card>
            </Col>

            <Col md={1}></Col>

            <Col md={7}>
              <Card className="mb-3">
                <Card.Body>
                  <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                    <h4>Stock Information</h4>
                    <Link className="nav-link" href={"/stocks/" + company_data["symbol"]}>
                      <Button variant="primary">More Info</Button>
                    </Link>
                  </div>
                  <p><strong>Stock Ticker:</strong> {company_data["symbol"]}</p>
                  <p><strong>Primary Share Holder:</strong> {stockssh_data["holder_name"]}</p>
                  <p><strong>Market Cap:</strong> {company_stock_data["market_cap"]} </p>
                  <p><strong>Last Closing Price:</strong> ${parseFloat(company_data["closing_price"]).toFixed(2)} </p>
                  <h4>Class Action Lawsuits</h4>
                  {/* <p><strong>Class Action Lawsuits:</strong> AHHH FIX ME </p> */}
                  <div className="dropdown">
                    <button
                        className="btn btn-secondary dropdown-toggle"
                        type="button"
                        id={`dropdownMenuButton-${companyName}`}
                        data-bs-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                    >
                        {lawsuits_data.length ?? 0} Class Action Lawsuits
                    </button>
                    <div
                        className="dropdown-menu"
                        aria-labelledby={`dropdownMenuButton-${companyName}`}
                        style={{ maxHeight: '200px', overflowY: 'auto' }}
                    >
                        {lawsuits_data.map((singleLawsuit) => {
                            // singleLawsuit is a dictionary with one key, the caseID
                            const caseID = Object.keys(singleLawsuit)[0]
                            return (
                                <a className="dropdown-item" href={`/lawsuits/${companyName}/${caseID}`}>
                                  {singleLawsuit[caseID]["name"].substring(0, 40)}
                                </a>
                            );
                        })}
                    </div>
                  </div>
                </Card.Body>
              </Card>

              {/* company description from wiki */}
              <Card className="mb-3">
                <Card.Body>
                  {company_data["description"]}
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  )
}

export default company


export async function getServerSideProps(context) {
  const { params } = context
  const companyName = params.company

  const data_calls = {}

  // loop through links to make requests
  for (let key in links) {
    // get link pairs, 0 is localhost, 1 is api
    const value = links[key]

    // try fetching data from the links
    const data = await fetchData(value);
    data_calls[key] = data;
  } 

  const company_data = data_calls["companies_urls"][companyName]
  const ticker = company_data["symbol"]
  const company_stock_data = data_calls["stocks_urls"][ticker]
  const company_stockssh_data = data_calls["stockssh_urls"][ticker]

  // list of dictionaries
  var company_lawsuits_data = data_calls["lawsuits_urls"][ticker]
  if (company_lawsuits_data === undefined) {
    company_lawsuits_data = []
  }

  // remove the [#]
  company_data["description"] = company_data["description"].replace(/\[\d+\]/g, '');

  // remove the (The) at the end of holder_name strings
  company_stockssh_data["holder_name"] = company_stockssh_data["holder_name"].replace(/ \(The\)$/, '');

  return {
      props: { 
          companyName: companyName,
          company_data : company_data, 
          company_stock_data: company_stock_data, 
          stockssh_data: company_stockssh_data,
          lawsuits_data: company_lawsuits_data
      }
  }

};
