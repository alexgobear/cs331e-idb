// pages/stock-chart.js
import { useState } from 'react';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
} from 'recharts';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

const formatDate = (dateStr) => {
  const date = new Date(dateStr);
  return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
};

const StockChart = () => {
  const [data, setData] = useState(null);

  const fetchData = async () => {
    // Replace with your actual API URL
    const apiUrl = 'https://api.classlessaction.me/stockPrice/MMM';

    try {
      const response = await fetch(apiUrl);
      const rawData = await response.json();
      const slicedData = rawData.slice(-4475)
      const fetchedData = slicedData.map((entry) => ({
        closing_price: entry.closing_price,
        date: entry.date,
        symbol: entry.symbol,
      }));
      setData(fetchedData);
    } catch (error) {
      console.error(error);
      setData([]);
    }
  };

  const chartData = data
    ? data.map((entry) => ({
        date: formatDate(entry.date),
        closingPrice: entry.closing_price,
      }))
    : [];

  return (
    <div>
      <h1>Stock Closing Prices</h1>
      <Button variant="primary" onClick={fetchData}>
        Fetch Data
      </Button>
      {data && (
        <Card style={{ marginTop: '1rem' }}>
          <Card.Body>
            <ResponsiveContainer width="100%" height={400}>
              <LineChart data={chartData} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
                <XAxis dataKey="date" />
                <YAxis />
                <CartesianGrid strokeDasharray="3 3" />
                <Tooltip />
                <Line type="monotone" dataKey="closingPrice" stroke="#8884d8" dot={false} />
              </LineChart>
            </ResponsiveContainer>
          </Card.Body>
        </Card>
      )}
    </div>
  );
};

export default StockChart;
