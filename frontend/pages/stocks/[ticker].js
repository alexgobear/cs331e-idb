import Navbar from '../../components/navBar'
import fetchData from '../../components/apiCall'
import Head from 'next/head'
import {useState, useEffect} from 'react'
import Link from 'next/link'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'

import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    ResponsiveContainer,
  } from 'recharts';

// they all use symbol as the common key
const companies_local = process.env.COMPANIES_LOCALHOST_URL
const companies_api = process.env.COMPANIES_API_URL
const stocks_local = process.env.STOCKS_LOCALHOST_URL
const stocks_api = process.env.STOCKS_API_URL
const stocksSH_local = process.env.STOCKS_SH_LOCALHOST_URL
const stocksSH_api = process.env.STOCKS_SH_API_URL
const lawsuits_local = process.env.LAWSUITS_LOCALHOST_URL
const lawsuits_api = process.env.LAWSUITS_API_URL

const links = {"companies_urls": [companies_api, companies_local], "stocks_urls": [ stocks_api, stocks_local], 
                "stockssh_urls": [stocksSH_api, stocksSH_local], "lawsuits_urls": [lawsuits_api, lawsuits_local]}


const formatDate = (dateStr) => {
    const date = new Date(dateStr);
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
};
    


const Stock = ({ ticker, companyName, company_data, company_stock_data, stockssh_data, lawsuits_data}) => {

    // info for plot
    const [data, setData] = useState(null);

    const fetchStockData = async () => {
      // Replace with your actual API URL
      const plotURL = `https://api.classlessaction.me/stockPrice/${ticker}`;
  
      try {
        const response = await fetch(plotURL);
        const rawData = await response.json();
        const slicedData = rawData.slice(-4475)
        const fetchedData = slicedData.map((entry) => ({
          closing_price: entry.closing_price,
          date: entry.date,
          symbol: entry.symbol,
        }));
        setData(fetchedData);
      } catch (error) {
        console.error(error);
        setData([]);
      }
    };

    const chartData = data
    ? data.map((entry) => ({
        date: formatDate(entry.date),
        closingPrice: entry.closing_price,
      }))
    : [];


    return (
        <>
            <Head>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <title>Classless Action</title>
            </Head>
            <div>
                <Navbar></Navbar>
                
                <Container style={{ paddingTop: '50px' }}>
                <Row className="pb-5">
                    {/* company description and picture */}
                    <Col md={4}>
                    <Card className="mb-3">
                        <Card.Img variant='top' src={'/images/companyLogos/' + company_data["symbol"] + '.png'}></Card.Img>
                        
                        <Link style={{ textDecoration: 'none', color: 'blue' }} href={"/companies/" + companyName}>
                        <Card.Title style={{'paddingTop': '10px'}}className="text-center">
                        <h3>{companyName}</h3>    
                        </Card.Title>                    
                        </Link>

                        <Card.Body>
                        <p><strong>Founded:</strong> {company_data["founded"]} </p>
                        <p><strong>Location:</strong> {company_data["location"]} </p>
                        <p><strong>Sector:</strong> {company_data["sector"]} </p>
                        <hr/>

                        <h4>Class Action Lawsuits</h4>
                        {/* <p><strong>Class Action Lawsuits:</strong> AHHH FIX ME </p> */}
                        <div className="dropdown">
                            <button
                                className="btn btn-secondary dropdown-toggle"
                                type="button"
                                id={`dropdownMenuButton-${companyName}`}
                                data-bs-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                            >
                                {lawsuits_data.length ?? 0} Class Action Lawsuits
                            </button>
                            <div
                                className="dropdown-menu"
                                aria-labelledby={`dropdownMenuButton-${companyName}`}
                                style={{ maxHeight: '200px', overflowY: 'auto' }}
                            >
                                {lawsuits_data.map((singleLawsuit) => {
                                    // singleLawsuit is a dictionary with one key, the caseID
                                    const caseID = Object.keys(singleLawsuit)[0]
                                    return (
                                        <a className="dropdown-item" href={`/lawsuits/${companyName}/${caseID}`}>
                                        {singleLawsuit[caseID]["name"].substring(0, 40)}
                                        </a>
                                    );
                                })}
                            </div>
                        </div>
                        </Card.Body>
                    </Card>
                    </Col>

                    <Col md={1}></Col>

                    <Col md={7}>
                    <Card className="mb-3">
                        <Card.Body>
                        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                            <h4>Stock Information</h4>
                            <Link className="nav-link" href={"/stocks/" + company_data["symbol"]}>
                            <Button variant="primary" onClick={fetchStockData}>View Stock Plot</Button>
                            </Link>
                        </div>
                        <p><strong>Stock Ticker:</strong> {company_data["symbol"]}</p>
                        <p><strong>Primary Share Holder:</strong> {stockssh_data["holder_name"]}</p>
                        <p><strong>Market Cap:</strong> {company_stock_data["market_cap"]} </p>
                        <p><strong>Last Closing Price:</strong> ${parseFloat(company_data["closing_price"]).toFixed(2)} </p>
                        </Card.Body>
                    </Card>

                        {data && (
                            <Card style={{ marginTop: '1rem' }}>
                            <Card.Body>
                                <ResponsiveContainer width="100%" height={400}>
                                <LineChart data={chartData} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
                                    <XAxis dataKey="date" />
                                    <YAxis />
                                    <CartesianGrid strokeDasharray="3 3" />
                                    <Tooltip />
                                    <Line type="monotone" dataKey="closingPrice" stroke="#8884d8" dot={false} />
                                </LineChart>
                                </ResponsiveContainer>
                            </Card.Body>
                            </Card>
                        )}

                    </Col>
                </Row>

                </Container>
            </div>
    </>
  )
}

export async function getServerSideProps(context) {
  const { params } = context
  const ticker = params.ticker

  const data_calls = {}

  // loop through links to make requests
  for (let key in links) {
      // get link pairs, 0 is localhost, 1 is api
      const value = links[key]

      // try fetching data from the links
      const data = await fetchData(value);
      data_calls[key] = data;
  }

  let companyName = ""
  for (const company in data_calls["companies_urls"]) {
    if (data_calls["companies_urls"][company].symbol == ticker){
        companyName = data_calls["companies_urls"][company].name;
        break;
    }
  }
  
  const company_data = data_calls["companies_urls"][companyName]
  const company_stock_data = data_calls["stocks_urls"][ticker]
  const company_stockssh_data = data_calls["stockssh_urls"][ticker]
  company_stockssh_data["holder_name"] = company_stockssh_data["holder_name"].replace(/ \(The\)$/, '');


  // list of dictionaries
  let company_lawsuits_data = data_calls["lawsuits_urls"][ticker]
  if (company_lawsuits_data === undefined) {
    company_lawsuits_data = []
  }


  // remove the (The) at the end of holder_name strings
  // company_stockssh_data["holder_name"] = company_stockssh_data["holder_name"].replace(/ \(The\)$/, '');

  return {
      props: {
          ticker: ticker,
          companyName: companyName,
          company_data : company_data, 
          company_stock_data: company_stock_data, 
          stockssh_data: company_stockssh_data,
          lawsuits_data: company_lawsuits_data
      }
  }

};

export default Stock