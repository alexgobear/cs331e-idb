import React from 'react'
import Head from 'next/head'
import Navbar from '../../components/navBar'
import fetchData from '../../components/apiCall'
import Link from 'next/link'
import { useState } from 'react'

import Table from 'react-bootstrap/Table'
import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'
import InputGroup from 'react-bootstrap/InputGroup'
import Form from 'react-bootstrap/Form'
import Card from 'react-bootstrap/Card'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import ToggleButton from 'react-bootstrap/ToggleButton'


const companies_local = process.env.COMPANIES_LOCALHOST_URL
const companies_api = process.env.COMPANIES_API_URL
const stocks_local = process.env.STOCKS_LOCALHOST_URL
const stocks_api = process.env.STOCKS_API_URL

const links = {"companies_urls": [companies_api, companies_local], "stocks_urls": [stocks_api, stocks_local]}


const stocks = ({ companies_data, stocks_data}) => {
    // Define state for the selected option and filter keyword
    const [sortBy, setSortBy] = useState("companyName");
    const [searchQuery, setSearchQuery] = useState("");

    
    const handleSort = (sortKey) => {
        setSortBy(sortKey);
    };

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
    };

    
    const filteredAndSortedCompaniesData = [...companies_data]
        .filter((company) => {
        if (!searchQuery) {
            return true;
        }

        const companyNameMatch = company[0].toLowerCase().includes(searchQuery.toLowerCase());
        const stockTickerMatch = company[1]["symbol"].toLowerCase().includes(searchQuery.toLowerCase());
        const sectorMatch = company[1]["sector"].toLowerCase().includes(searchQuery.toLowerCase());

        return (searchQuery && (companyNameMatch || stockTickerMatch || sectorMatch))
        })

        .sort((a, b) => {
        const companyA = a;
        const companyB = b;
        
        if (sortBy === "companyName") {
            return companyA[0].localeCompare(companyB[0]);
        } else if (sortBy === "stockTicker") {
            return companyA[1]["symbol"].localeCompare(companyB[1]["symbol"]);
        } else if (sortBy === "classActionLawsuits") {
            if (companyA[1]["num_lawsuits"] > companyB[1]["num_lawsuits"]) {
                return -1
            } else if (companyA[1]["num_lawsuits"] < companyB[1]["num_lawsuits"]) {
                return 1
            } else {
                return 0
            }
        } else if (sortBy == "closingPrice") {
            return (companyA[1]["closing_price"] < companyB[1]["closing_price"]) ? 1 : (companyA[1]["closing_price"] > companyB[1]["closing_price"]) ? -1 : 0;
        } else {
            // Default sorting by company name
            return companyA[0].localeCompare(companyB[0]);
        }
        })


    const colTitles = ["Company", "Sector", "Total Lawsuits", "Stock Ticker", "Last Closing Price", "Market Cap"]
    const [radioValue, setRadioValue] = useState('1');
  
    const radios = [
      { name: 'Company Name', value: '1', sortKey: 'companyName' },
      { name: 'Stock Ticker', value: '2', sortKey: 'stockTicker' },
      { name: 'Number of Class Action Lawsuits', value: '3', sortKey: 'classActionLawsuits' },
      {name: 'Closing Price', value: '4', sortKey: 'closingPrice'}
    ];

    return (
        <>
            <Head>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <title>Classless Action</title>
            </Head>
  
            <div>
                <Navbar></Navbar>
                <div class="container pt-5 px-5 pb-3">
                  <Card>
                    <div class="row">
                      <div class="align-items-center justify-content-center">
                        {/* <input type="text" value={searchQuery} onChange={handleSearchChange} placeholder="Search" /> */}
                        <div class="container pt-3 d-flex align-items-center justify-content-center">
                          <h3>
                            Sort By
                          </h3>
                        </div>
                        <div className="container pt-3 text-center">
                          <div className="row">
                            <div className="col">
                              <div className="px-3">
                                <ButtonGroup>
                                  {radios.map((radio, idx) => (
                                    <ToggleButton
                                      key={idx}
                                      id={`radio-${idx}`}
                                      type="radio"
                                      variant={'outline-primary'}
                                      name="radio"
                                      value={radio.value}
                                      checked={radioValue === radio.value}
                                      onClick={() => handleSort(radio.sortKey)}
                                      onChange={(e) => setRadioValue(e.currentTarget.value)}
                                    >
                                      {radio.name}
                                    </ToggleButton>
                                  ))}
                                </ButtonGroup>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="pb-3">
                        <div class="container pt-3 d-flex align-items-center justify-content-center">
                          <InputGroup className="my-2">
                            <InputGroup.Text>Search:</InputGroup.Text>
                            <Form.Control
                              type="text"
                              value={searchQuery}
                              onChange={handleSearchChange}
                              placeholder="Search for a Company, Stock Ticker, or Sector"
                            />
                          </InputGroup>
                        </div>
                      </div>
                    </div>
                  </Card>
                </div>

                <Container>
                    <Table hover>
                    <thead>
                        <tr>
                            {colTitles.map(title => {
                                return (<th>{title}</th>)
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {filteredAndSortedCompaniesData.map(company => {
                            const ticker = company[1]["symbol"]
                            const company_stock_data = stocks_data[ticker]

                            return (
                                <tr>
                                    <td>
                                    <Link className="nav-link" style={{ textDecoration: 'none', color: 'blue' }} href={"companies/" + company[0]}>{company[0]}</Link>
                                    </td>
                                    <td>{company[1]["sector"]}</td>
                                    <td>
                                        <Link className='nav-link' style={{ textDecoration: 'none', color: 'blue'}} href={"lawsuits"}>{company[1]["num_lawsuits"]}</Link>
                                    </td>
                                    <td>
                                        <Link className='nav-link' style={{ textDecoration: 'none', color: 'blue'}} href={"stocks/" + ticker}>{ticker}</Link>
                                    </td>
                                    <td>{parseFloat(company[1]["closing_price"]).toFixed(2)}</td>
                                    <td>{company_stock_data["market_cap"]}</td>
                                </tr>
                                
                            )
                        })}
                    </tbody>
                    </Table>

                </Container>
            </div>
        </>
      )
}


export async function getServerSideProps() {
    const data_calls = {}

    // loop through links to make requests
    for (let key in links) {
      // get link pairs, 0 is localhost, 1 is api
      const value = links[key]

      // try fetching data from the links
      const data = await fetchData(value);
      data_calls[key] = data;
    } 


    data_calls["companies_urls"] = Object.entries(data_calls["companies_urls"]);

    return {
        props: { 
            companies_data : data_calls["companies_urls"] , 
            stocks_data: data_calls["stocks_urls"]
        }
    }

};


export default stocks