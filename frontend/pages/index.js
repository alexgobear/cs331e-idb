import React from 'react'
import Navbar from '../components/navBar'
import Head from 'next/head'
import styles from '../styles/index.module.css'
import companiesImg from '../public/images/misc/companies.png'
import lawsuitsImg from '../public/images/misc/lawsuits.png'
import stocksImg from '../public/images/misc/stocks.png'
import { Carousel } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'


const Index = () => {

    return (
        <>
            <Head>
                {/* <meta charset="utf-8" /> */}
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <title>Classless Action</title>
            </Head>

            <div>
                <Navbar></Navbar>
                {/* <Container fluid style={{paddingLeft: '75px', paddingTop: '25px', paddingRight: '75px'}}> */}
                <Container fluid style={{height: "calc(100vh - 200px)"}}>
                    <div className="row align-items-center no-gutters" >
                        {/* carousel */}
                        <div className="col-9">
                            <Carousel id="splashCarousel" interval={4000} pause={'hover'} fade>
                                <Carousel.Item className={styles["carousel-item"]}>
                                    <div
                                        className={styles["overlay-image"]}
                                        style={{
                                            backgroundImage: `url(${companiesImg.src})`,
                                        }}
                                    />
                                    <Carousel.Caption>
                                        <h1>Companies</h1>
                                        <p>View basic information about companies in our data set.</p>
                                        <a className="btn btn-primary btn-lg" href="/companies" role="button">
                                        Go to Companies</a>
                                    </Carousel.Caption>
                                </Carousel.Item>
                                <Carousel.Item className={styles["carousel-item"]}>
                                    <div
                                        className={styles["overlay-image"]}
                                        style={{
                                            backgroundImage: `url(${lawsuitsImg.src})`,
                                        }}
                                    />
                                    <Carousel.Caption>
                                        <h1>Lawsuits</h1>
                                        <p>Access our list of class action lawsuits.</p>
                                        <a className="btn btn-primary btn-lg" href="/lawsuits" role="button">
                                            Go to Lawsuits</a>
                                    </Carousel.Caption>
                                </Carousel.Item>
                                <Carousel.Item className={styles["carousel-item"]}>
                                    <div
                                        className={styles["overlay-image"]}
                                        style={{
                                            backgroundImage: `url(${stocksImg.src})`,
                                        }}
                                    />
                                    <Carousel.Caption>
                                        <h1>Stocks</h1>
                                        <p>See how previous legal action has affected a company's share price.</p>
                                        <a className="btn btn-primary btn-lg" href="/stocks" role="button">
                                        Go to Stocks</a>
                                    </Carousel.Caption>
                                </Carousel.Item>
                            </Carousel>
                        </div>

                        {/* welcome card */}
                        <div className="col-3">
                            <div className="card">
                                <div className="card-body">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">
                                            <h2 className="card-title display-6 py-2" style={{textAlign: "center"}}>Welcome to Classless Action</h2>
                                        </li>
                                        <li className="list-group-item">
                                            <h3 className="card-text py-3">
                                                Our site examines the connection between companies, the class action lawsuits filed against them, and their
                                                stock price.
                                            </h3>
                                        </li>
                                        <li className="list-group-item">
                                            <h5 className="card-text">
                                                Are you curious which companies have had the most legal action taken against them?
                                            </h5>
                                            <h5 className="card-text">
                                                Would you like to know whether losing a class action lawsuit affects a companies
                                                valuation in the short or long term?
                                            </h5>
                                            <h5 class="card-text">Hopefully, we can help you answer these sorts of questions.</h5>
                                            <h5 class="card-text">
                                                <strong>Just use the navigation bar at the top of the page to get started, or check
                                                    out the carousel on the left for our suggestions.
                                                </strong>
                                            </h5>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </Container>
            </div>
        </>
    )
}



export default Index