

ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
    DOC := docker run -it -v $$(PWD):/usr/cs330e -w /usr/cs330e fareszf/python
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python                 # on my machine it's python
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := python -m coverage
    PYDOC    := python -m pydoc        # on my machine it's pydoc
    AUTOPEP8 := autopep8
else                                   # UTCS
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
endif

log:
	git log > IDB3.log

GCP-Deploy:
	git pull
	gcloud app deploy

clearDB:
	psql -d classless_action -U postgres -f backend/sql_scripts/drop_all.sql

TestModels.tmp:
	$(COVERAGE) run    --branch backend/test_models.py >  TestModels.tmp 2>&1
	$(COVERAGE) report -m                      >> TestModels.tmp

clean:
	rm -rf */__pycache__
	rm -rf *.tmp