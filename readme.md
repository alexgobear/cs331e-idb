# Classless Action  

## Specifications  

- [Phase 1](https://www.cs.utexas.edu/~fares/cs331es23/CS%20373_files/projects/IDB1.html)
- [Phase 2](https://www.cs.utexas.edu/~fares/cs331es23/CS%20373_files/projects/IDB2.html)
- [Phase 3](https://www.cs.utexas.edu/~fares/cs331es23/CS%20373_files/projects/IDB3.html)
  
## Dependencies  

Recommended to use a virtual environment  
Project created and tested with:  

- Python 3.9.7 and pip 23.0  
- Postgres 15.2  
- Node.js 16.13.0  
  
## Database Setup  
  
### Install PostgreSQL (v15)  

- keep default port (5432)  
- select command line tools  
- password: password  
- set as env var  
  
### Create the database

Command Prompt  
$ psql -U postgres  
$ password: password  
$ CREATE database classless_action;  

### To Empty the Database  

$ make clearDB  
or
$ psql -d classless_action -U postgres -f backend/sql_scripts/drop_all.sql  
  
## Backend Setup  
  
### Virtual Env Setup

Project Root Directory  

1. $ virtualenv venv  
2. $ python -m venv venv  
  
### Venv Activation/Deactivation  

. venv/Scripts/activate  
deactivate  
  
### Install Required Python Packages  

Project Root Directory  
Activate Venv  

1. pip install -r requirements.txt  
2. python -m pip install -r requirements.txt  
  
## Frontend Setup  
  
### Install Required Packages  

Frontend Directory  
$ npm install  
  
## Start the Development Servers  
  
### Start the Backend  

Backend Directory  
$ python main.py  
  
### Populate the Database  

[http://127.0.0.1:5000/populate](http://127.0.0.1:5000/populate)  
  
### Frontend

Frontend Directory  
$ npm run dev  
  